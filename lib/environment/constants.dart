import 'package:flutter/material.dart';

class Constants {
  static const String SERVER_IP_ADDRESS = '10.0.2.2';
  static const String BASE_URL = 'http://' + SERVER_IP_ADDRESS + ':8080/sysponics';
  static const int MQTT_PORT = 1883;
  static const String MQTT_BROKER = SERVER_IP_ADDRESS;
  static const String MQTT_IDENTIFIER = 'sysponics-app';
  static const String OAUTH_BASIC='c3lzcG9uaWNzLXNlcnZlcjpzeXNwb25pY3MyMDE5';


  static const String PREFS_REFRESH_TOKEN = 'refresh_token';
  static const String PREFS_ACCESS_TOKEN = 'access_token';
  static const String PREFS_USER_DATA = 'user_data';

  static const int DEFAULT_PAGE_SIZE = 10;

  static const IconData IC_AIR_TEMPERATURE = Icons.ac_unit;
  static const IconData IC_AIR_HUMIDITY = Icons.water_damage;
  static const IconData IC_WATER_TEMPERATURE = Icons.device_thermostat;
  static const IconData IC_SPECIES = Icons.grass;
  static const IconData IC_CALENDAR = Icons.calendar_today;
  static const IconData IC_SCHEDULE = Icons.schedule;
  static const IconData IC_DEVICE = Icons.settings_input_component;
  static const IconData IC_PROJECT = Icons.local_florist;
  static const IconData IC_LIGHTS = Icons.wb_incandescent;
  static const IconData IC_WATER_PUMP = Icons.waves;
  static const IconData IC_DEVICE_ON = Icons.toggle_on;
  static const IconData IC_DEVICE_OFF = Icons.toggle_off;
  static const IconData IC_HISTORY = Icons.history;
}
