// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'speciesdto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SpeciesDTO _$SpeciesDTOFromJson(Map<String, dynamic> json) {
  return SpeciesDTO(
    json['id'] as int,
    json['cientificName'] as String,
    json['commonName'] as String,
  );
}

Map<String, dynamic> _$SpeciesDTOToJson(SpeciesDTO instance) =>
    <String, dynamic>{
      'id': instance.id,
      'cientificName': instance.cientificName,
      'commonName': instance.commonName,
    };
