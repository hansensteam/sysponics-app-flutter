import 'package:json_annotation/json_annotation.dart';

part 'speciesdto.g.dart';

@JsonSerializable(disallowUnrecognizedKeys: false, ignoreUnannotated: true)
class SpeciesDTO {

  @JsonKey(name: "id")
  int id;
  @JsonKey(name: "cientificName")
  String cientificName;
  @JsonKey(name: "commonName")
  String commonName;


  SpeciesDTO(this.id, this.cientificName, this.commonName);

  @override
  Map<String, dynamic> toMap() => _$SpeciesDTOToJson(this);

  factory SpeciesDTO.fromJson(Map<String, dynamic> json) => _$SpeciesDTOFromJson(json);
}