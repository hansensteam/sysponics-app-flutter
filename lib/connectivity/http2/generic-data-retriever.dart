import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sysponics/environment/constants.dart';
import 'package:sysponics/model/GenericModel.dart';
import 'package:sysponics/persistence/dao/GenericDao.dart';

abstract class GenericDataRetriever<T extends GenericModel, U extends GenericDao> {
  int retries = 0;

  String endpoint();

  String collection();

  T jsonToT(Map<String, dynamic> json);

  Map<String, dynamic> tToJson(T obj);

  U dao();

  String resourceUrl = Constants.BASE_URL;

  Future<bool> refresh_token() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    String url = '$resourceUrl/oauth/token';
    http.Response response = await http.post(
      url,
      headers: {
        'Authorization': 'Basic ${Constants.OAUTH_BASIC}',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: {
        'grant_type': 'refresh_token',
        'refresh_token': sp.getString(Constants.PREFS_REFRESH_TOKEN),
      },
    );
    if (response.statusCode == 200) {
      Map<String, dynamic> json = jsonDecode(response.body);
      sp.setString(Constants.PREFS_ACCESS_TOKEN, json['access_token']);
    } else {
      throw HttpException('Status ${response.statusCode} not expected!\n${response.body}', uri: Uri.parse(url));
    }
    return response.statusCode == 200;
  }

  Future<Map<String, dynamic>> recordCount() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.get(Constants.PREFS_ACCESS_TOKEN);
    String url = resourceUrl + '/${endpoint()}?page=0&size=${Constants.DEFAULT_PAGE_SIZE}';
    http.Response response = await http.get(url, headers: {'Authorization': 'Bearer $token'});
    if (response.statusCode == 200) {
      return jsonDecode(response.body)['page'];
    } else if (response.statusCode == 401 && retries < 3) {
      bool isOk = await refresh_token();
      retries++;
      return recordCount();
    } else {
      return {'totalElements': 0, 'totalPages': 0};
    }
  }

  Future<List<T>> findAll(int page) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    String token = sp.get(Constants.PREFS_ACCESS_TOKEN);
    String url = resourceUrl + '/${endpoint()}?page=$page&size=${Constants.DEFAULT_PAGE_SIZE}';
    http.Response response = await http.get(url, headers: {'Authorization': 'Bearer $token'});
    if (response.statusCode == 200) {
      List<T> _response = List.empty(growable: true);
      (jsonDecode(response.body)['_embedded'][collection()] as List).forEach((element) {
        _response.add(jsonToT(element));
      });
      return _response;
    } else if (response.statusCode == 204) {
      return [];
    } else if (response.statusCode == 401 && retries < 3) {
      bool isOk = await refresh_token();
      retries++;
      return findAll(page);
    } else {
      throw HttpException('Status ${response.statusCode} not expected!\n${response.body}', uri: Uri.parse(url));
    }
  }

  Future persistChildren(List<Map<String, dynamic>> parents);

  Future<List<T>> findAllAndPersist() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    String token = sp.get(Constants.PREFS_ACCESS_TOKEN);
    String url = resourceUrl + '/${endpoint()}';
    http.Response response = await http.get(url, headers: {'Authorization': 'Bearer $token'});

    if (response.statusCode == 200) {
      List<T> _response = List.empty(growable: true);
      List<Map<String, dynamic>> parents = List.empty(growable: true);
      (jsonDecode(response.body)['_embedded'][collection()] as List).forEach((element) {
        parents.add(element);
        _response.add(jsonToT(element));
      });
      for (T t in _response) {
        await this.dao().insert(t);
      }
      await this.persistChildren(parents);
      return _response;
    } else if (response.statusCode == 204) {
      return [];
    } else if (response.statusCode == 401 && retries < 3) {
      bool isOk = await refresh_token();
      retries++;
      return findAllAndPersist();
    } else {
      throw HttpException('Status ${response.statusCode} not expected!\n${response.body}', uri: Uri.parse(url));
    }
  }

  Future<bool> deleteById(int id) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    String token = sp.get(Constants.PREFS_ACCESS_TOKEN);
    String fullUrl = resourceUrl + '/${endpoint()}/remove/$id';
    http.Response response = await http.delete(fullUrl, headers: {'Authorization': 'Bearer $token'});
    if (response.statusCode == 401 && retries < 3) {
      bool isOk = await refresh_token();
      retries++;
      return deleteById(id);
    } else {
      return response.statusCode == 200;
    }
  }

  Future<T> save(T obj) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    String token = sp.get(Constants.PREFS_ACCESS_TOKEN);
    String url = resourceUrl + '/${endpoint()}/persist';
    Map<String, dynamic> json = tToJson(obj);
    Map<String, dynamic> user = jsonDecode(sp.get(Constants.PREFS_USER_DATA));
    json.putIfAbsent('usuario', () => {'id': user['id'] as int});
    http.Response response = await http.post(url, headers: {'Authorization': 'Bearer $token', 'Content-Type': 'application/json'}, body: jsonEncode(json));
    switch (response.statusCode) {
      case 200:
      case 201:
        return jsonToT(jsonDecode(response.body));
      case 401:
        if (retries < 3) {
          bool isOk = await refresh_token();
          retries++;
          return save(obj);
        } else {
          throw HttpException('Status ${response.statusCode} not expected!\n${response.body}', uri: Uri.parse(url));
        }
        break;
      default:
        throw HttpException('Status ${response.statusCode} not expected!\n${response.body}', uri: Uri.parse(url));
    }
  }

  Future<T> findById(int id) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    String token = sp.get(Constants.PREFS_ACCESS_TOKEN);
    String url = resourceUrl + '/${endpoint()}/by-id/$id';
    http.Response response = await http.get(url, headers: {'Authorization': 'Bearer $token'});
    if (response.statusCode == 200) {
      T _response = jsonToT(jsonDecode(response.body));
      return _response;
    } else if (response.statusCode == 204) {
      return null;
    } else if (response.statusCode == 401 && retries < 3) {
      bool isOk = await refresh_token();
      retries++;
      return findById(id);
    } else {
      throw HttpException('Status ${response.statusCode} not expected!\n${response.body}', uri: Uri.parse(url));
    }
  }
}
