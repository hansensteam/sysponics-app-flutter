import 'package:sysponics/connectivity/http2/generic-data-retriever.dart';
import 'package:sysponics/model/project/Project.dart';
import 'package:sysponics/persistence/dao/ProjectDao.dart';

class ProjectDataRetriever extends GenericDataRetriever<Project, ProjectDao> {
  @override
  String endpoint() {
    return 'projects';
  }

  @override
  String collection() {
    return 'projects';
  }

  @override
  Project jsonToT(Map<String, dynamic> json) {
    return Project.fromJson(json);
  }

  @override
  ProjectDao dao() {
    return ProjectDao();
  }

  @override
  Future persistChildren(List<Map<String, dynamic>> parents) async {}

  @override
  Map<String, dynamic> tToJson(Project obj) {
    Map<String, dynamic> map = obj.toMap();
    map.putIfAbsent('species', () => {'id': obj.speciesId});
    return map;
  }
}
