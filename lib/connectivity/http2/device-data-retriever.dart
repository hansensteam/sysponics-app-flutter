import 'package:sysponics/connectivity/http2/generic-data-retriever.dart';
import 'package:sysponics/model/device/Device.dart';
import 'package:sysponics/persistence/dao/DeviceDao.dart';

class DeviceDataRetriever extends GenericDataRetriever<Device, DeviceDao> {
  @override
  String collection() {
    return 'devices';
  }

  @override
  DeviceDao dao() {
    return DeviceDao();
  }

  @override
  String endpoint() {
    return 'devices';
  }

  @override
  Device jsonToT(Map<String, dynamic> json) {
    return Device.fromJson(json);
  }

  @override
  Future persistChildren(List<Map<String, dynamic>> parents) async {}

  @override
  Map<String, dynamic> tToJson(Device obj) {
    return obj.toMap();
  }
}
