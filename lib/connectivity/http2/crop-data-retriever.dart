import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sysponics/connectivity/http2/generic-data-retriever.dart';
import 'package:sysponics/environment/constants.dart';
import 'package:sysponics/model/crop/Crop.dart';
import 'package:sysponics/model/dto/crop-last-data/crop-last-data.dart';
import 'package:sysponics/persistence/dao/crop/CropDao.dart';
import 'package:intl/intl.dart';

class CropDataRetriever extends GenericDataRetriever<Crop, CropDao> {
  @override
  String collection() {
    return 'crops';
  }

  @override
  CropDao dao() {
    return CropDao();
  }

  @override
  String endpoint() {
    return 'crops';
  }

  @override
  Crop jsonToT(Map<String, dynamic> json) {
    return Crop.fromJson(json);
  }

  Future<Crop> getActiveCrop(String projectKey) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    String token = sp.get(Constants.PREFS_ACCESS_TOKEN);
    String url = resourceUrl + '/${endpoint()}/$projectKey/active';
    http.Response response = await http.get(url, headers: {'Authorization': 'Bearer $token'});
    if (response.statusCode == 200 && response.body.isNotEmpty) {
      return jsonToT(jsonDecode(response.body));
    } else if (response.statusCode == 204 || (response.statusCode == 200 && response.body.isEmpty)) {
      return null;
    } else if (response.statusCode == 401 && retries < 3) {
      bool isOk = await refresh_token();
      retries++;
      return getActiveCrop(projectKey);
    } else {
      throw HttpException('Status ${response.statusCode} not expected!\n${response.body}', uri: Uri.parse(url));
    }
  }

  Future<CropLastDataDTO> getLastData(int cropId) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    String token = sp.get(Constants.PREFS_ACCESS_TOKEN);
    String url = resourceUrl + '/${endpoint()}/last-data/$cropId';
    http.Response response = await http.get(url, headers: {'Authorization': 'Bearer $token'});
    if (response.statusCode == 200) {
      return CropLastDataDTO.fromJson(jsonDecode(response.body));
    } else if (response.statusCode == 204) {
      return null;
    } else if (response.statusCode == 401 && retries < 3) {
      bool isOk = await refresh_token();
      retries++;
      return getLastData(cropId);
    } else {
      throw HttpException('Status ${response.statusCode} not expected!\n${response.body}', uri: Uri.parse(url));
    }
  }

  Future<List<Crop>> getCropsOfProject(String projectId) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    String token = sp.get(Constants.PREFS_ACCESS_TOKEN);
    String url = resourceUrl + '/${endpoint()}/$projectId/all';
    http.Response response = await http.get(url, headers: {'Authorization': 'Bearer $token'});
    if (response.statusCode == 200) {
      List rList = jsonDecode(response.body);
      List<Crop> list = List.empty(growable: true);
      for (Map<String, dynamic> map in rList) {
        list.add(Crop.fromJson(map));
      }
      return list;
    } else if (response.statusCode == 204) {
      return [];
    } else if (response.statusCode == 401 && retries < 3) {
      bool isOk = await refresh_token();
      retries++;
      return getCropsOfProject(projectId);
    } else {
      throw HttpException('Status ${response.statusCode} not expected!\n${response.body}', uri: Uri.parse(url));
    }
  }

  Future<Crop> saveCrop(Crop crop, int projectId) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    String token = sp.get(Constants.PREFS_ACCESS_TOKEN);
    String url = resourceUrl + '/${endpoint()}/persist';
    Map<String, dynamic> json = tToJson(crop);
    Map<String, dynamic> user = jsonDecode(sp.get(Constants.PREFS_USER_DATA));
    json.putIfAbsent('usuario', () => {'id': user['id'] as int});
    json.putIfAbsent('project', () => {'id': projectId});
    http.Response response = await http.post(url, headers: {'Authorization': 'Bearer $token', 'Content-Type': 'application/json'}, body: jsonEncode(json));
    switch (response.statusCode) {
      case 200:
      case 201:
        return jsonToT(jsonDecode(response.body));
      case 401:
        if (retries < 3) {
          bool isOk = await refresh_token();
          retries++;
          return saveCrop(crop, projectId);
        } else {
          throw HttpException('Status ${response.statusCode} not expected!\n${response.body}', uri: Uri.parse(url));
        }
        break;
      default:
        throw HttpException('Status ${response.statusCode} not expected!\n${response.body}', uri: Uri.parse(url));
    }
  }

  Future<bool> markAsHarvested(int cropId, int speciesId, DateTime date, String obs) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    String token = sp.get(Constants.PREFS_ACCESS_TOKEN);
    String url = resourceUrl + '/${endpoint()}/mark-harvested';
    http.Response response = await http.post(url, headers: {'Authorization': 'Bearer $token', 'Content-Type': 'application/x-www-form-urlencoded'}, body: {'crop-id': cropId, 'species-id': speciesId, 'harvest-date': DateFormat('dd/MM/yyyy').format(date), 'obs': obs});
    switch (response.statusCode) {
      case 200:
      case 201:
        return response.body as bool;
      case 401:
        if (retries < 3) {
          bool isOk = await refresh_token();
          retries++;
          return markAsHarvested(cropId, speciesId, date, obs);
        } else {
          throw HttpException('Status ${response.statusCode} not expected!\n${response.body}', uri: Uri.parse(url));
        }
        break;
      default:
        throw HttpException('Status ${response.statusCode} not expected!\n${response.body}', uri: Uri.parse(url));
    }
  }

  @override
  Future persistChildren(List<Map<String, dynamic>> parents) async {}

  @override
  Map<String, dynamic> tToJson(Crop obj) {
    return obj.toMap();
  }
}
