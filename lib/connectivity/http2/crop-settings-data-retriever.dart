import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sysponics/connectivity/http2/generic-data-retriever.dart';
import 'package:sysponics/environment/constants.dart';
import 'package:sysponics/model/crop/Crop.dart';
import 'package:sysponics/model/crop_schedules/CropSchedules.dart';
import 'package:sysponics/model/crop_settings/CropSettings.dart';
import 'package:sysponics/persistence/dao/crop/CropSettingsDao.dart';

class CropSettingsDataRetriever extends GenericDataRetriever<CropSettings, CropSettingsDao> {
  @override
  String collection() {
    return "cropSettings";
  }

  @override
  CropSettingsDao dao() {
    return CropSettingsDao();
  }

  @override
  String endpoint() {
    return "cropSettings";
  }

  @override
  CropSettings jsonToT(Map<String, dynamic> json) {
    return CropSettings.fromJson(json);
  }

  @override
  Future persistChildren(List<Map<String, dynamic>> parents) {
    // TODO: implement persistChildren
    throw UnimplementedError();
  }

  @override
  Map<String, dynamic> tToJson(CropSettings obj) {
    return obj.toMap();
  }

  Future<CropSettings> findByCropId(int cropId) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    String token = sp.get(Constants.PREFS_ACCESS_TOKEN);
    String url = resourceUrl + '/${endpoint()}/find-by-crop-id/$cropId';
    http.Response response = await http.get(url, headers: {'Authorization': 'Bearer $token'});
    if (response.statusCode == 200 && response.body.isNotEmpty) {
      Map<String, dynamic> json = jsonDecode(response.body);
      List schedules = json['schedules'];
      CropSettings cs = jsonToT(json);
      if (schedules != null) {
        schedules.forEach((element) => cs.schedules.add(CropSchedules.fromJson(element)));
      }
      return cs;
    } else if (response.statusCode == 204) {
      return null;
    } else {
      throw HttpException('Status ${response.statusCode} not expected!\n${response.body}', uri: Uri.parse(url));
    }
  }

  Future<CropSettings> saveCropSettings(Crop crop, CropSettings settings, List<CropSchedules> schedules) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    String token = sp.getString(Constants.PREFS_ACCESS_TOKEN);
    String url = resourceUrl + '/${endpoint()}/persist';
    Map<String, dynamic> json = settings.toMap();
    List<Map<String, dynamic>> lSch = List.empty(growable: true);
    schedules.forEach((element) => lSch.add(element.toMap()));
    json['schedules'] = lSch;
    json['crop'] = {'id': settings.cropId};
    String encoded = jsonEncode(json);
    http.Response response = await http.post(
      url,
      headers: {
        'Authorization': 'Bearer $token',
        'Content-Type': 'application/json',
      },
      body: encoded,
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      return CropSettings.fromJson(jsonDecode(response.body));
    } else if (response.statusCode == 401 && retries < 3) {
      bool isOk = await refresh_token();
      retries++;
      return saveCropSettings(crop, settings, schedules);
    } else {
      throw HttpException('Status ${response.statusCode} not expected!\n${response.body}', uri: Uri.parse(url));
    }
  }
}
