import 'dart:convert';
import 'dart:io';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:sysponics/connectivity/http2/generic-data-retriever.dart';
import 'package:sysponics/dto/speciesdto.dart';
import 'package:sysponics/environment/constants.dart';
import 'package:sysponics/model/species/Species.dart';
import 'package:sysponics/model/species_names/SpeciesNames.dart';
import 'package:sysponics/persistence/dao/species/SpeciesDao.dart';
import 'package:sysponics/persistence/dao/species/SpeciesNamesDao.dart';
import 'package:http/http.dart' as http;

class SpeciesDataRetriever extends GenericDataRetriever<Species, SpeciesDao> {
  @override
  String collection() {
    return 'species';
  }

  @override
  String endpoint() {
    return 'species';
  }

  @override
  Species jsonToT(Map<String, dynamic> json) {
    Species species = Species.fromJson(json);
    List<SpeciesNames> names = List.empty(growable: true);
    List lNames = json['commonNames'];
    lNames.forEach((name) => names.add(SpeciesNames.fromJson(name)));
    species.commonNames = names;
    return species;
  }

  @override
  SpeciesDao dao() {
    return SpeciesDao();
  }

  @override
  Future persistChildren(List<Map<String, dynamic>> parents) async {
    List<SpeciesNames> names = List.empty(growable: true);
    parents.forEach((element) {
      (element['commonNames'] as List).forEach((snEl) {
        SpeciesNames name = SpeciesNames.fromJson(snEl);
        name.speciesId = element['id'] as int;
        names.add(name);
      });
    });
    SpeciesNamesDao dao = SpeciesNamesDao();
    for (SpeciesNames name in names) {
      await dao.insert(name);
    }
  }

  Future<List<SpeciesDTO>> speciesOfLocale({String locale = 'pt_br', int page = -1}) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    String token = sp.getString(Constants.PREFS_ACCESS_TOKEN);
    String url = '$resourceUrl/${endpoint()}/of-locale?locale=$locale';
    if (page >= 0) {
      url += '&page=$page&size=${Constants.DEFAULT_PAGE_SIZE}';
    }
    http.Response response = await http.get(
      url,
      headers: {'Authorization': 'Bearer $token'},
    );
    if (response.statusCode == 200) {
      List<SpeciesDTO> dtos = List.empty(growable: true);
      List map = jsonDecode(response.body);
      map.forEach((element) {
        dtos.add(SpeciesDTO.fromJson(element));
      });
      return dtos;
    } else if (response.statusCode == 204) {
      return [];
    } else if (response.statusCode == 401 && retries < 3) {
      bool isOk = await refresh_token();
      retries++;
      return speciesOfLocale(locale: locale, page: page);
    } else {
      throw HttpException('Status ${response.statusCode} not expected', uri: Uri.parse(url));
    }
  }

  @override
  Map<String, dynamic> tToJson(Species obj) {
    List names = List.empty(growable: true);
    obj.commonNames.forEach((name) => names.add(name.toMap()));
    Map<String, dynamic> json = obj.toMap();
    json['commonNames'] = names;
    return json;
  }

  Future<SpeciesDTO> speciesOfLocaleById(int id, {String locale = 'pt_br', int page = -1}) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    String token = sp.getString(Constants.PREFS_ACCESS_TOKEN);
    String url = '$resourceUrl/${endpoint()}/of-locale-by-id/$id?locale=$locale';
    http.Response response = await http.get(
      url,
      headers: {'Authorization': 'Bearer $token'},
    );
    if (response.statusCode == 200) {
      SpeciesDTO dtos = SpeciesDTO.fromJson(jsonDecode( response.body));
      return dtos;
    } else if (response.statusCode == 204) {
      return null;
    } else if (response.statusCode == 401 && retries < 3) {
      bool isOk = await refresh_token();
      retries++;
      return speciesOfLocaleById( id, locale: locale);
    } else {
      throw HttpException('Status ${response.statusCode} not expected', uri: Uri.parse(url));
    }
  }
}
