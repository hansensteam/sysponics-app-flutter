import 'package:sysponics/model/crop_schedules/CropSchedules.dart';
import 'package:sysponics/persistence/dao/crop/CropSchedulesDao.dart';

import 'generic-data-retriever.dart';

class CropSchedulesDataRetriever extends GenericDataRetriever<CropSchedules, CropSchedulesDao> {
  @override
  String collection() {
    return 'cropSchedules';
  }

  @override
  CropSchedulesDao dao() {
    return CropSchedulesDao();
  }

  @override
  String endpoint() {
    return 'cropSchedules';
  }

  @override
  CropSchedules jsonToT(Map<String, dynamic> json) {
    return CropSchedules.fromJson(json);
  }

  @override
  Future persistChildren(List<Map<String, dynamic>> parents) {
    // TODO: implement persistChildren
    throw UnimplementedError();
  }

  @override
  Map<String, dynamic> tToJson(CropSchedules obj) {
    return obj.toMap();
  }

}