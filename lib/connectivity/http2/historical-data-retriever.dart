import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sysponics/connectivity/http2/generic-data-retriever.dart';
import 'package:sysponics/environment/constants.dart';
import 'package:sysponics/model/data/HistoricalData.dart';
import 'package:sysponics/persistence/dao/HistoricalDataDao.dart';

class HistoricalDataRetriever extends GenericDataRetriever<HistoricalData, HistoricalDataDao> {
  @override
  String collection() {
    return 'historical-data';
  }

  @override
  HistoricalDataDao dao() {
    return HistoricalDataDao();
  }

  Future<List<HistoricalData>> findAllByCropId(int cropId, int page) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    String token = sp.get(Constants.PREFS_ACCESS_TOKEN);
    String url = resourceUrl + '/${endpoint()}/search/findAllByCrop_Id?cropid=$cropId&size=${Constants.DEFAULT_PAGE_SIZE}&page=$page&sort=id+desc';
    http.Response response = await http.get(url, headers: {'Authorization': 'Bearer $token'});

    if (response.statusCode == 200) {
      List<HistoricalData> _response = List.empty(growable: true);
      (jsonDecode(response.body)['_embedded'][collection()] as List).forEach((element) {
        _response.add(jsonToT(element));
      });
      return _response;
    } else if (response.statusCode == 204) {
      return [];
    } else if (response.statusCode == 401 && retries < 3) {
      bool isOk = await refresh_token();
      retries++;
      return findAllByCropId(cropId, page);
    } else {
      throw HttpException('Status ${response.statusCode} not expected!\n${response.body}', uri: Uri.parse(url));
    }
  }

  @override
  String endpoint() {
    return 'historical-data';
  }

  @override
  HistoricalData jsonToT(Map<String, dynamic> json) {
    return HistoricalData.fromJson(json);
  }

  @override
  Future persistChildren(List<Map<String, dynamic>> parents) async {}

  @override
  Map<String, dynamic> tToJson(HistoricalData obj) {
    return obj.toMap();
  }
}
