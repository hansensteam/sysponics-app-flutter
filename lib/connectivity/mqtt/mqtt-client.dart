
import 'package:mqtt_client/mqtt_client.dart';
import 'package:mqtt_client/mqtt_server_client.dart';
import 'package:sysponics/environment/constants.dart';

class MqttClient {
  final client =
  MqttServerClient(Constants.MQTT_BROKER, Constants.MQTT_IDENTIFIER);

  Future<MqttServerClient> connect() async {
    client.logging(on: true);
    client.onConnected = onConnected;
    client.onDisconnected = onDisconnected;
    client.onSubscribed = onSubscribed;
    client.onSubscribeFail = onSubscribeFail;
    client.onUnsubscribed = onUnsubscribed;
    client.pongCallback = pong;

    // final prefs = await SharedPreferences.getInstance();
    // LoggedUser user = LoggedUser.fromJson(
    //     jsonDecode(prefs.get(Constants.PREFS_USER_DATA)));


    final connMessage = MqttConnectMessage()
        .authenticateAs('pnktyzmy', '5fzaDbMmpFxN')
        .withClientIdentifier(Constants.MQTT_IDENTIFIER + '_' )
        .keepAliveFor(60)
        .withWillTopic('/will')
        .withWillMessage('teste')
        .startClean()
        .withWillQos(MqttQos.atLeastOnce);

    print('MQTT START CONNECTION');
    client.connectionMessage = connMessage;
    try {
      await client.connect();
    } catch (e) {
      print('Exception connecting to MQTT $e');
      client.disconnect();
    }

    if (client.connectionStatus.state == MqttConnectionState.connected) {
      print('connected');
    } else {
      print('error connecting ${client.connectionStatus}');
      client.disconnect();
      return null;
    }

    client.updates.listen((event) {
      print(event);
    });

    // client.updates.listen((List<MqttReceivedMessage<MqttMessage>> c) {
    //   final MqttPublishMessage message = c[0].payload;
    //   final payload =
    //   MqttPublishPayload.bytesToStringAsString(message.payload.message);
    //   print('Received message: $payload from topic ${c[0].topic}');
    // });
    return client;
  }

  subscribe() {

  }

  void onConnected() {
    print('connected');
  }

  void onDisconnected() {
    print('disconnected');
  }

  void onSubscribed(String topic) {
    print('subscribed:' + topic);
  }

  void onSubscribeFail(String topic) {
    print('subscribe failed: ' + topic);
  }

  void onUnsubscribed(String topic) {
    print('unsubscribed:' + topic);
  }

  void pong() {
    print('pong');
  }
}
