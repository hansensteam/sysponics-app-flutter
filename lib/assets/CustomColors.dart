import 'dart:ui';

import 'package:flutter/material.dart';

class CustomColors {
  static const Color APP_BRAND_COLOR = Color.fromARGB(255, 95, 170, 94); //Color.fromRGBO(95, 170, 94, 1.0);
  static const Color APP_BACKGROUND = Colors.white;//Color.fromARGB(255, 255, 246, 226); //Color.fromRGBO(255, 246, 226, 1.0);
  static const Color APP_ACCENT1 = Color.fromARGB(255, 246, 248, 247); // Color.fromRGBO(246, 248, 247, 1.0);
  static const Color DISABLED = Colors.grey;
}
