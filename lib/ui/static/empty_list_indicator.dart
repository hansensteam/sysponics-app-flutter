import 'package:flutter/cupertino.dart';

class EmptyListIndicator extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 32, horizontal: 16),
        child: Column(
          children: [
            Text(
              'Sem Dados!',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            Text('A lista está vazia!'),
          ],
        ),
      ),
    );
  }

}