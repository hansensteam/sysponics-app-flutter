import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:sysponics/assets/CustomColors.dart';
import 'package:sysponics/connectivity/http2/device-data-retriever.dart';
import 'package:sysponics/environment/constants.dart';
import 'package:sysponics/model/device/Device.dart';
import 'package:sysponics/ui/devices/device-form.dart';
import 'package:sysponics/ui/static/empty_list_indicator.dart';
import 'package:sysponics/ui/widgets/side-menu/side-menu.dart';

class DeviceListForm extends StatefulWidget {
  static const route = '/device-list';

  @override
  State<StatefulWidget> createState() {
    return DeviceListFormState();
  }
}

class DeviceListFormState extends State<DeviceListForm> {
  final PagingController<int, Device> _pagingController = PagingController(firstPageKey: 0);

  @override
  void initState() {
    _pagingController.addPageRequestListener((pageKey) {
      fetchPage(pageKey);
    });
    super.initState();
  }

  void fetchPage(int page) {
    DeviceDataRetriever().findAll(page).then((list) {
      final isLastPage = list.length < Constants.DEFAULT_PAGE_SIZE;
      if (isLastPage) {
        _pagingController.appendLastPage(list);
      } else {
        _pagingController.appendPage(list, page + 1);
      }
    }).catchError((error) => _pagingController.error = error);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.APP_BACKGROUND,
      appBar: AppBar(
        title: Text('Dispositivos'),
      ),
      drawer: SideMenu(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        backgroundColor: CustomColors.APP_BRAND_COLOR,
        foregroundColor: CustomColors.APP_BACKGROUND,
        onPressed: () {
          Navigator.pushNamed(context, DeviceForm.route).then((value) => _pagingController.refresh());
        },
      ),
      body: RefreshIndicator(
        onRefresh: () => Future.sync(
          () => _pagingController.refresh(),
        ),
        child: PagedListView<int, Device>(
          pagingController: _pagingController,
          builderDelegate: PagedChildBuilderDelegate<Device>(
            noItemsFoundIndicatorBuilder: (context) => EmptyListIndicator(),
            itemBuilder: (context, item, index) {
              return Slidable(
                actionPane: SlidableDrawerActionPane(),
                actionExtentRatio: 0.25,
                child: ListTile(
                  leading: CircleAvatar(
                    backgroundColor: CustomColors.APP_BRAND_COLOR,
                    foregroundColor: CustomColors.APP_BACKGROUND,
                    child: Icon(Constants.IC_DEVICE),
                  ),
                  title: Text('${item.description} (${item.name})'),
                ),
                secondaryActions: [
                  IconSlideAction(
                    color: Colors.blue,
                    caption: 'Edit',
                    icon: Icons.edit,
                    onTap: () {
                      Navigator.pushNamed(
                        context,
                        DeviceForm.route,
                        arguments: item,
                      ).then((value) => _pagingController.refresh());
                    },
                  ),
                  IconSlideAction(
                    color: Colors.red,
                    caption: 'Delete',
                    icon: Icons.delete,
                    onTap: () {
                      deleteClick(item);
                    },
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }

  deleteClick(Device device) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
              title: Text('Confirme'),
              actions: [
                TextButton(
                    child: Text('Sim'),
                    onPressed: () {
                      Navigator.of(context).pop();
                      DeviceDataRetriever().deleteById(device.id).then((b) {
                        _pagingController.refresh();
                      }).catchError((error) {
                        Scaffold.of(context).showSnackBar(
                          SnackBar(
                            content: Text(error),
                          ),
                        );
                      });
                    }),
                TextButton(
                  child: Text('Não'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
              content: SingleChildScrollView(
                child: ListBody(
                  children: [Text('Deseja realmente excluir o dispositivo ${device.name}?')],
                ),
              ));
        });
  }
}
