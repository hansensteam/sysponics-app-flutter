import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sysponics/assets/CustomColors.dart';
import 'package:sysponics/connectivity/http2/device-data-retriever.dart';
import 'package:sysponics/model/device/Device.dart';
import 'package:sysponics/ui/widgets/side-menu/side-menu.dart';
import 'package:uuid/uuid.dart';

class DeviceForm extends StatefulWidget {
  static const String route = '/device-form';

  @override
  State<StatefulWidget> createState() {
    return DeviceFormState();
  }
}

class DeviceFormState extends State<DeviceForm> {
  Device _device = Device(name: 'Novo Dispositivo');
  final TextEditingController _nameFilter = TextEditingController();
  final TextEditingController _descricaoFilter = TextEditingController();
  final _deviceFormKey = GlobalKey<FormState>();

  DeviceFormState() {
    _nameFilter.addListener(() {
      _device.name = _nameFilter.text.isEmpty ? '' : _nameFilter.text;
    });
    _descricaoFilter.addListener(() {
      _device.description = _descricaoFilter.text.isEmpty ? '' : _descricaoFilter.text;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (ModalRoute.of(context).settings.arguments != null) {
      _device = ModalRoute.of(context).settings.arguments;
      _nameFilter.text = _device.name;
      _descricaoFilter.text = _device.description;
    } else {
      Uuid uuid = Uuid();
      String sKey = uuid.v4();
      sKey = sKey.replaceAll('-', '');
      sKey = sKey.substring(0, 16);
      print(sKey);
      _device.key = sKey;
    }

    return Scaffold(
      backgroundColor: CustomColors.APP_BACKGROUND,
      appBar: AppBar(
        title: Text(_device.name.isNotEmpty ? _device.name : 'Novo Dispositivo'),
        actions: [
          IconButton(
            icon: Icon(
              Icons.save,
              color: CustomColors.APP_BACKGROUND,
            ),
            onPressed: () {
              DeviceDataRetriever().save(_device).then((value) {
                print(value.toMap());
                Navigator.pop(context);
              }).catchError((error) {
                Scaffold.of(context).showSnackBar(
                  SnackBar(
                    content: Text(error),
                  ),
                );
              });
            },
          ),
        ],
      ),
      drawer: SideMenu(),
      body: Form(
        key: _deviceFormKey,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            child: Column(
              children: [
                TextFormField(
                  controller: _nameFilter,
                  decoration: InputDecoration(labelText: 'Nome'),
                ),
                TextFormField(
                  controller: _descricaoFilter,
                  decoration: InputDecoration(labelText: 'Descrição'),
                ),
                DropdownButton<String>(
                  isExpanded: true,
                  value: _device.deviceType,
                  hint: Text('Tipo de Dispositivo'),
                  items: DeviceType.values.map((type) {
                    return DropdownMenuItem(
                      value: type.value,
                      child: Text(type.description),
                    );
                  }).toList(),
                  onChanged: (type) {
                    setState(() {
                      _device.deviceType = type;
                    });
                  },
                ),
                Text('O código abaixo será utilizado para a integração com o hardware.'),
                Text('Ele deverá ser informado manualmente no código do microcontrolador.'),
                Builder(builder: (BuildContext context) {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(_device.key),
                      IconButton(
                        icon: Icon(Icons.copy),
                        onPressed: () {
                          Clipboard.setData(ClipboardData(text: _device.key));
                          Scaffold.of(context).showSnackBar(SnackBar(content: Text('Texto Copiado para a Área de Transferência')));
                        },
                      )
                    ],
                  );
                }),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
