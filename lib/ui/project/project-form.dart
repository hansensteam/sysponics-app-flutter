import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sysponics/connectivity/http2/project-data-retriever.dart';
import 'package:sysponics/connectivity/http2/species-data-retriever.dart';
import 'package:sysponics/dto/speciesdto.dart';
import 'package:sysponics/model/project/Project.dart';
import 'package:sysponics/model/species/Species.dart';
import 'package:sysponics/model/species_names/SpeciesNames.dart';
import 'package:sysponics/persistence/dao/species/SpeciesDao.dart';
import 'package:sysponics/persistence/dao/species/SpeciesNamesDao.dart';
import 'package:sysponics/ui/widgets/side-menu/side-menu.dart';

class ProjectForm extends StatefulWidget {
  static const route = '/project-form';

  @override
  State<StatefulWidget> createState() {
    return ProjectFormState();
  }
}

class ProjectFormState extends State<ProjectForm> {
  Project _project = Project();
  final TextEditingController _nameFilter = TextEditingController();
  final _projectFormKey = GlobalKey<FormState>();

  ProjectFormState() {
    _nameFilter.addListener(_nameListen);
  }

  void _nameListen() {
    if (_nameFilter.text.isEmpty) {
      _project.name = '';
    } else {
      _project.name = _nameFilter.text;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Novo Projeto'),
        actions: [
          IconButton(
            icon: Icon(Icons.save),
            onPressed: () {
              print(_project.toMap());
              ProjectDataRetriever().save(_project).then((value) {
                print(value.toMap());
                Navigator.pop(context);
              }).catchError((error) {
                Scaffold.of(context).showSnackBar(
                  SnackBar(
                    content: Text(error),
                  ),
                );
              });
            },
          ),
        ],
      ),
      drawer: SideMenu(),
      body: Form(
        key: _projectFormKey,
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10.0),
          child: Column(
            children: [
              TextFormField(
                controller: _nameFilter,
                decoration: InputDecoration(labelText: 'Nome'),
              ),
              FutureBuilder(
                future: SpeciesDataRetriever().speciesOfLocale(),
                builder: (BuildContext context, AsyncSnapshot<List<SpeciesDTO>> snapshot) {
                  if (!snapshot.hasData) {
                    return CircularProgressIndicator();
                  }
                  return DropdownButton<int>(
                    isExpanded: true,
                    value: _project.speciesId,
                    hint: Text('Espécie'),
                    items: snapshot.data.map((species) {
                      return DropdownMenuItem<int>(
                        value: species.id,
                        child: Text(species.commonName + ' (' + species.cientificName + ')'),
                      );
                    }).toList(),
                    onChanged: (species) {
                      setState(() {
                        _project.speciesId = species;
                      });
                    },
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
