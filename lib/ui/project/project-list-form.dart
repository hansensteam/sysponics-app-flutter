import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:sysponics/assets/CustomColors.dart';
import 'package:sysponics/connectivity/http2/project-data-retriever.dart';
import 'package:sysponics/connectivity/http2/species-data-retriever.dart';
import 'package:sysponics/environment/constants.dart';
import 'package:sysponics/model/project/Project.dart';
import 'package:sysponics/ui/project/project-data-card.dart';
import 'package:sysponics/ui/project/project-form.dart';
import 'package:sysponics/ui/static/empty_list_indicator.dart';
import 'file:///E:/PROJECTS/Sysponics/sysponics_app_flutter/lib/ui/project/project-page/project-page.dart';
import 'package:sysponics/ui/tempHumChart.dart';
import 'package:sysponics/ui/widgets/CustomCircularProgressIndicator.dart';
import 'package:sysponics/ui/widgets/side-menu/side-menu.dart';

class ProjectListForm extends StatefulWidget {
  static const route = '/project-list';

  @override
  State<StatefulWidget> createState() {
    return ProjectListFormState();
  }
}

class ProjectListFormState extends State<ProjectListForm> {
  final PagingController<int, Project> _pagingController = PagingController(firstPageKey: 0);

  bool _modalLoading = false;

  @override
  void initState() {
    _pagingController.addPageRequestListener((pageKey) {
      fetchPage(pageKey);
    });
    super.initState();
  }

  void fetchPage(int page) {
    ProjectDataRetriever().findAll(page).then((list) {
      final isLastPage = list.length < Constants.DEFAULT_PAGE_SIZE;
      if (isLastPage) {
        _pagingController.appendLastPage(list);
      } else {
        _pagingController.appendPage(list, page + 1);
      }
    }).catchError((error) {
      _pagingController.error = error;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.APP_BACKGROUND,
      appBar: AppBar(
        title: Text('Projetos'),
        actions: [
          IconButton(
            icon: Icon(Icons.cloud_download, color: CustomColors.APP_BACKGROUND),
            onPressed: fetchData,
          )
        ],
      ),
      drawer: SideMenu(),
      body: RefreshIndicator(
        onRefresh: () => Future.sync(
          () => _pagingController.refresh(),
        ),
        child: PagedListView<int, Project>(
          pagingController: _pagingController,
          builderDelegate: PagedChildBuilderDelegate<Project>(
            itemBuilder: (context, item, index) {
              return Slidable(
                actionPane: SlidableDrawerActionPane(),
                actionExtentRatio: 0.25,
                child: GestureDetector(
                  child: Column(
                    children: [
                      ProjectDataCard(project: item),
                    ],
                  ),
                  onTap: () {
                    Navigator.pushNamed(context, ProjectPage.route, arguments: item);
                  },
                ),
                secondaryActions: [
                  IconSlideAction(
                    color: Colors.red,
                    caption: 'Excluir',
                    icon: Icons.delete,
                    onTap: () {
                      deleteButtonClick(item);
                    },
                  )
                ],
              );
            },
            noItemsFoundIndicatorBuilder: (context) => EmptyListIndicator(),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, ProjectForm.route).then((value) => _pagingController.refresh());
        },
        child: Icon(Icons.add),
        backgroundColor: CustomColors.APP_BRAND_COLOR,
      ),
    );
  }

  void deleteButtonClick(Project project) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
              title: Text('Confirme'),
              actions: [
                TextButton(
                    child: Text('Sim'),
                    onPressed: () {
                      Navigator.of(context).pop();
                      ProjectDataRetriever().deleteById(project.id).then((b) {
                        _pagingController.refresh();
                      }).catchError((error) {
                        Scaffold.of(context).showSnackBar(
                          SnackBar(
                            content: Text(error),
                          ),
                        );
                      });
                    }),
                TextButton(
                  child: Text('Não'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
              content: SingleChildScrollView(
                child: ListBody(
                  children: [Text('Deseja realmente excluir o projeto ${project.name}?')],
                ),
              ));
        });
  }

  fetchData() {
    setState(() {
      _modalLoading = true;
    });
    SpeciesDataRetriever().findAllAndPersist().then((value) {
      print(value.length);
      setState(() {
        _modalLoading = false;
      });
    });
  }
}
