import 'package:date_field/date_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:sysponics/assets/CustomColors.dart';
import 'package:sysponics/connectivity/http2/crop-data-retriever.dart';
import 'package:sysponics/connectivity/http2/crop-settings-data-retriever.dart';
import 'package:sysponics/environment/constants.dart';
import 'package:sysponics/model/crop/Crop.dart';
import 'package:sysponics/model/crop_schedules/CropSchedules.dart';
import 'package:sysponics/model/crop_settings/CropSettings.dart';
import 'package:sysponics/model/project/Project.dart';
import 'package:intl/intl.dart';

class ProjectSettingsPage extends StatefulWidget {
  static const route = '/project-settings';

  @override
  State<StatefulWidget> createState() {
    return ProjectSettingsPageState();
  }
}

class ProjectSettingsPageState extends State<ProjectSettingsPage> {
  Project _project;
  Crop _crop;
  CropSettings _settings = CropSettings(millisToVerify: 30);
  List<CropSchedules> _schedules = List.empty(growable: true);

  final _formKey = GlobalKey<FormState>();
  final _alertKey = GlobalKey<FormState>();
  TextEditingController _millisToVerify = TextEditingController();
  TextEditingController _waterPh = TextEditingController();

  @override
  void initState() {
    _millisToVerify.addListener(_listenToMillis);
    _waterPh.addListener(_listenToPH);
    _millisToVerify.text = '${_settings.millisToVerify}';
    super.initState();
  }

  getCropSettings() {
    CropDataRetriever().getActiveCrop(_project.key).then((value) {
      if (value != null) {
        _crop = value;
        _settings.cropId = value.id;
        CropSettingsDataRetriever().findByCropId(_crop.id).then((cs) {
          if (cs != null) {
            setState(() {
              _settings = cs;
              _millisToVerify.text = '${(_settings.millisToVerify / 1000).round()}';
              _waterPh.text = '${_settings.waterPh}';
              _schedules = _settings.schedules;
            });
          }
        }).catchError((error) {
          print(error);
          Scaffold.of(context).showSnackBar(
            SnackBar(
              content: Text(error),
            ),
          );
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_project == null) {
      _project = ModalRoute.of(context).settings.arguments;
    }
    if (_crop == null) {
      getCropSettings();
    }
    return Scaffold(
      appBar: AppBar(
        title: Text('Configurações do Projeto'),
        actions: [
          Builder(
            builder: (context) {
              return IconButton(
                icon: Icon(Icons.save),
                onPressed: () {
                  if (this._formKey.currentState.validate()) {
                    _settings.millisToVerify = int.parse(_millisToVerify.text) * 1000;
                    CropSettingsDataRetriever().saveCropSettings(_crop, _settings, _schedules).then((value) {
                      if (value != null) {
                        Navigator.pop(context);
                      }
                    }).catchError((error) {
                      print( error );
                      Scaffold.of(context).showSnackBar(
                        SnackBar(
                          content: Text(error),
                        ),
                      );
                    });
                  }
                },
              );
            },
          )
        ],
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              TextFormField(
                controller: _millisToVerify,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(labelText: 'Verificar a cada (segundos)'),
                validator: (value) {
                  int dMillis = value.isEmpty ? 0 : int.parse(value);
                  return dMillis > 0 ? null : 'Informe o tempo para verificar';
                },
              ),
              TextFormField(
                controller: _waterPh,
                keyboardType: TextInputType.numberWithOptions(decimal: true),
                decoration: InputDecoration(labelText: 'PH a ser mantido'),
                validator: (value) {
                  double dPH = value.isEmpty ? -1 : double.parse(value);
                  return dPH < 0 || dPH > 14 ? 'O valor deve estar entre 0 e 14' : null;
                },
              ),
              FlatButton(
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(right: 8),
                      child: Icon(Icons.alarm),
                    ),
                    Text('Adicionar Configuração'),
                  ],
                ),
                onPressed: () {
                  configureSchedule(CropSchedules());
                },
              ),
              Divider(),
              Expanded(
                child: ListView.builder(
                  itemCount: _schedules.length,
                  itemBuilder: (context, index) {
                    CropSchedules s = _schedules[index];
                    if (s.poweredOn == null) {
                      s.poweredOn = false;
                    }
                    DateFormat df = DateFormat('HH:mm:ss');
                    return Slidable(
                      actionPane: SlidableDrawerActionPane(),
                      actionExtentRatio: 0.25,
                      secondaryActions: [
                        IconSlideAction(
                          icon: Icons.delete,
                          caption: 'Excluir',
                          color: Colors.red,
                          onTap: () {
                            setState(() {
                              _schedules.remove(s);
                            });
                          },
                        ),
                      ],
                      child: ListTile(
                        leading: Icon(Icons.alarm),
                        title: Text(s.scheduleType.description),
                        subtitle: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text('${df.format(s.startTime)} - ${df.format(s.endTime)}'),
                                tracingElement(s),
                              ],
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget tracingElement(CropSchedules s) {
    switch (s.scheduleType) {
      case ScheduleType.WATER_PUMP:
      case ScheduleType.LIGHTS:
        return Icon(
          s.poweredOn ? Icons.toggle_on : Icons.toggle_off,
          color: s.poweredOn ? CustomColors.APP_BRAND_COLOR : Colors.black,
        );
      case ScheduleType.WATER_TEMP:
      case ScheduleType.AIR_TEMP:
        return Text('${s.value} ºC');
      case ScheduleType.AIR_HUM:
        return Text('${s.value} %');
    }
  }

  void configureSchedule(CropSchedules schedule) {
    schedule.poweredOn = false;
    String errorText = '';
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text('Cancelar'),
              ),
              TextButton(
                child: Text('OK'),
                onPressed: () {
                  if (_alertKey.currentState.validate()) {
                    setState(() {
                      _schedules.add(schedule);
                    });
                    Navigator.of(context).pop();
                  }
                },
              ),
            ],
            content: Form(
              key: _alertKey,
              child: StatefulBuilder(builder: (BuildContext context, StateSetter setState) {
                return SingleChildScrollView(
                  child: Column(
                    children: [
                      if (errorText.isNotEmpty) Text(errorText),
                      DropdownButtonFormField<ScheduleType>(
                        isExpanded: true,
                        hint: Text('Tipo'),
                        value: schedule.scheduleType,
                        items: ScheduleType.values
                            .map(
                              (val) => DropdownMenuItem<ScheduleType>(
                                child: Text(val.description),
                                value: val,
                              ),
                            )
                            .toList(),
                        onChanged: (type) => setState(() => schedule.scheduleType = type),
                        validator: (value) => value == null ? 'Tipo não pode ser vazio!' : null,
                      ),
                      DateTimeFormField(
                        mode: DateFieldPickerMode.time,
                        label: 'Hora Inicial',
                        enabled: true,
                        onDateSelected: (DateTime date) => setState(() => schedule.startTime = date),
                        validator: (date) => date == null ? 'Data de Início não pode ser vazia' : null,
                      ),
                      DateTimeFormField(
                        mode: DateFieldPickerMode.time,
                        label: 'Hora final',
                        enabled: true,
                        onDateSelected: (DateTime date) => setState(() => schedule.endTime = date),
                        validator: (date) => date == null ? 'Data de Término não pode ser vazia!' : null,
                      ),
                      valueWidget(schedule, setState),
                    ],
                  ),
                );
              }),
            ),
          );
        });
  }

  valueWidget(CropSchedules schedule, StateSetter setState) {
    switch (schedule.scheduleType) {
      case ScheduleType.WATER_PUMP:
      case ScheduleType.LIGHTS:
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(schedule.poweredOn ? 'Ligado' : 'Desligado'),
            Switch(
              value: schedule.poweredOn,
              activeColor: CustomColors.APP_BRAND_COLOR,
              onChanged: (value) {
                setState(() {
                  schedule.poweredOn = value;
                });
              },
            ),
          ],
        );
      case ScheduleType.WATER_TEMP:
      case ScheduleType.AIR_TEMP:
      case ScheduleType.AIR_HUM:
        return TextFormField(
          keyboardType: TextInputType.numberWithOptions(decimal: true),
          decoration: InputDecoration(labelText: 'Valor'),
          onChanged: (value) {
            double dValue = value.isEmpty ? 0 : double.parse(value);
            setState(() {
              schedule.value = dValue;
            });
          },
          validator: (value) {
            double dValue = value.isEmpty ? -1 : double.parse(value);
            return dValue < 0 ? 'O valor deve ser positivo' : null;
          },
        );
      default:
        return Divider();
    }
  }

  void _listenToMillis() {
    int dMillis = _millisToVerify.text.isEmpty ? 0 : int.parse(_millisToVerify.text);
    _settings.millisToVerify = dMillis;
  }

  void _listenToPH() {
    double dPH = _waterPh.text.isEmpty ? 0 : double.parse(_waterPh.text);
    _settings.waterPh = dPH;
  }
}
