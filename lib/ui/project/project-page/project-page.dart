import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:sysponics/assets/CustomColors.dart';
import 'package:sysponics/connectivity/http2/crop-data-retriever.dart';
import 'package:sysponics/connectivity/http2/species-data-retriever.dart';
import 'package:sysponics/dto/speciesdto.dart';
import 'package:sysponics/environment/constants.dart';
import 'package:sysponics/model/crop/Crop.dart';
import 'package:sysponics/model/data/HistoricalData.dart';
import 'package:sysponics/model/dto/crop-last-data/crop-last-data.dart';
import 'package:sysponics/model/project/Project.dart';
import 'package:sysponics/model/species/Species.dart';
import 'package:sysponics/model/species_names/SpeciesNames.dart';
import 'package:sysponics/persistence/dao/species/SpeciesDao.dart';
import 'package:sysponics/persistence/dao/species/SpeciesNamesDao.dart';
import 'package:sysponics/ui/project/project-page/project-harvest-crop.dart';
import 'package:sysponics/ui/project/project-page/project-new-crop-page.dart';
import 'package:sysponics/ui/project/project-page/project-page-crops-historical-data-widget.dart';
import 'package:sysponics/ui/project/project-page/project-page-crops-widget.dart';
import 'package:sysponics/ui/project/project-page/project-page-side-menu.dart';
import 'package:sysponics/ui/widgets/side-menu/side-menu.dart';

class ProjectPage extends StatefulWidget {
  static const route = '/project-page';

  @override
  State<StatefulWidget> createState() {
    return ProjectPageState();
  }
}

class ProjectPageState extends State<ProjectPage> {
  bool _loading = false;
  Project _project;
  Crop _crop;
  List<HistoricalData> _data = List.empty(growable: true);

  @override
  void initState() {
    setState(() {
      _loading = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    _project = ModalRoute
        .of(context)
        .settings
        .arguments;
    return Scaffold(
      backgroundColor: CustomColors.APP_BACKGROUND,
      appBar: AppBar(
        title: Text(_project.name),
        actions: [
          Builder(
            builder: (context) {
              return IconButton(
                icon: Icon(
                  Icons.more_vert,
                  color: CustomColors.APP_BACKGROUND,
                ),
                onPressed: () {
                  Scaffold.of(context).openEndDrawer();
                },
                tooltip: MaterialLocalizations
                    .of(context)
                    .openAppDrawerTooltip,
              );
            },
          )
        ],
      ),
      drawer: SideMenu(),
      endDrawer: ProjectSideMenu(
        project: _project,
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            speciesWidget(context),
            Divider(),
            buildCrops(context),
          ],
        ),
      ),
    );
  }

  FutureBuilder<List<SpeciesDTO>> speciesWidget(BuildContext cxt) {
    return FutureBuilder(
        future: SpeciesDataRetriever().speciesOfLocale(),
        builder: (BuildContext context, AsyncSnapshot<List<SpeciesDTO>> snapshot) {
          Widget w = ListTile(
            leading: Icon(Icons.block),
            title: Text('Sem dados de Espécie'),
          );
          if (snapshot.hasData) {
            for (SpeciesDTO dto in snapshot.data) {
              if (dto.id == _project.speciesId) {
                w = ListTile(
                  leading: CircleAvatar(
                    backgroundColor: CustomColors.APP_BRAND_COLOR,
                    foregroundColor: CustomColors.APP_BACKGROUND,
                    child: Icon(
                      Constants.IC_SPECIES,
                    ),
                  ),
                  title: Text(dto.commonName),
                  subtitle: Text(dto.cientificName),
                );
              }
            }
          }
          return w;
        });
  }

  FutureBuilder<Crop> buildCrops(BuildContext cxt) {
    return FutureBuilder(
      future: CropDataRetriever().getActiveCrop(_project.key),
      builder: (BuildContext context, AsyncSnapshot<Crop> snapshot) {
        if (snapshot.hasData) {
          _crop = snapshot.data;
          return hasCropData(context);
        } else {
          return noCropData(context);
        }
      },
    );
  }

  Widget hasCropData(BuildContext context) {
    return Expanded(
      child: ListView(
        children: [
          ListTile(
            leading: Icon(Constants.IC_CALENDAR),
            title: Text('Data de Plantio'),
            subtitle: Text(_crop.plantingDate != null ? DateFormat('dd/MM/yyyy').format(_crop.plantingDate) : 'Não definido'),
          ),
          ListTile(
            leading: Icon(Constants.IC_SCHEDULE),
            title: Text('Colheita estimada para'),
            subtitle: Text(_crop.expectedHarvestDate != null ? DateFormat('dd/MM/yyyy').format(_crop.expectedHarvestDate) : 'Não definido'),
          ),
          RaisedButton(
            child: Row(
              children: [Icon(Constants.IC_HISTORY), Text('Visualizar Medições'),],
            ),
            onPressed: () {
              Navigator.pushNamed(context, CropHistoricalDataWidget.route, arguments: _crop.id);
            },
          ),
          RaisedButton(
            child: Row(
              children: [Icon(Icons.agriculture), Text('Finalizar (Colheita)'),],
            ),
            onPressed: () {
              Navigator.pushNamed(context, ProjectHarvestCropPage.route, arguments: {
                'crop': _crop,
                'project': _project,
              }).then((value) => setState(() {}));
            },
          ),
          // RaisedButton(
          //   child: Row(
          //     children: [Icon(Icons.agriculture), Text('Finalizar (Colheita)'),],
          //   ),
          //   onPressed: () {
          //     Navigator.pushNamed(context, ProjectHarvestCropPage.route, arguments: {
          //       'crop': _crop,
          //       'project': _project,
          //     })
          //   },
          // )
          // ListTile(
          //   leading: Icon(Constants.IC_HISTORY),
          //   title: Text('Visualizar Medições'),
          //   onTap: () {},
          // ),
          // ListTile(
          //   leading: Icon(Icons.agriculture),
          //   title: Text('Finalizar (Colheita)'),
          //   onTap: () {
          //     Navigator.pushNamed(context, ProjectHarvestCropPage.route, arguments: {
          //       'crop': _crop,
          //       'project': _project,
          //     }).then((value) => setState(() {}));
          //   },
          // ),
          Divider(),
          historicalDataWidget(context, _crop)
        ],
      ),
    );
  }

  Widget noCropData(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            'Sem dados de Plantio',
            style: TextStyle(fontStyle: FontStyle.italic),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.symmetric(horizontal: 10.0),
                child: RaisedButton(
                  child: Text('Configurar Plantio'),
                  onPressed: () {
                    Navigator.pushNamed(context, ProjectNewCropPage.route, arguments: _project).then((value) => setState(() => _project = _project));
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 10.0),
                child: RaisedButton(
                  child: Text('Consultar Plantios'),
                  onPressed: () {
                    Navigator.pushNamed(context, ProjectPageCropsWidget.route, arguments: _project.key);
                  },
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget historicalDataWidget(BuildContext cxt, Crop crop) {
    return FutureBuilder(
      future: CropDataRetriever().getLastData(crop.id),
      builder: (BuildContext context, AsyncSnapshot<CropLastDataDTO> snapshot) {
        if (snapshot.hasData) {
          CropLastDataDTO dto = snapshot.data;
          return Column(
            children: [
              Text('Últimas Medições'),
              ListTile(
                leading: Icon(Constants.IC_AIR_TEMPERATURE),
                title: Text('Temperatura do Ar'),
                subtitle: Text('${dto.airTemp} ºC'),
              ),
              ListTile(
                leading: Icon(Constants.IC_AIR_HUMIDITY),
                title: Text('Umidade do Ar'),
                subtitle: Text('${dto.airHum} %'),
              ),
              ListTile(
                leading: Icon(Constants.IC_WATER_TEMPERATURE),
                title: Text('Temperatura da Água'),
                subtitle: Text('${dto.waterTemp} ºC'),
              ),
              ListTile(
                leading: Icon(Constants.IC_WATER_TEMPERATURE),
                title: Text('PH da Água'),
                subtitle: Text('${dto.waterPh}'),
              ),
              ListTile(
                leading: Icon(Constants.IC_LIGHTS),
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Iluminação'),
                    Icon(
                      dto.lightsOn ? Constants.IC_DEVICE_ON : Constants.IC_DEVICE_OFF,
                      color: dto.lightsOn ? CustomColors.APP_BRAND_COLOR : CustomColors.DISABLED,
                    ),
                  ],
                ),
              ),
              ListTile(
                leading: Icon(Constants.IC_WATER_PUMP),
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Bomba d\'água'),
                    Icon(
                      dto.pumpOn ? Constants.IC_DEVICE_ON : Constants.IC_DEVICE_OFF,
                      color: dto.pumpOn ? CustomColors.APP_BRAND_COLOR : CustomColors.DISABLED,
                    ),
                  ],
                ),
                subtitle: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Nível da água',
                      style: TextStyle(color: dto.waterLevelOk ? Colors.black : Colors.redAccent),
                    ),
                    Icon(
                      dto.waterLevelOk ? Constants.IC_DEVICE_ON : Constants.IC_DEVICE_OFF,
                      color: dto.waterLevelOk ? CustomColors.APP_BRAND_COLOR : Colors.redAccent,
                    ),
                  ],
                ),
              ),
            ],
          );
        } else {
          return Text('Sem dados para mostrar');
        }
      },
    );
  }
}
