import 'package:flutter/material.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:intl/intl.dart';
import 'package:sysponics/connectivity/http2/crop-data-retriever.dart';
import 'package:sysponics/environment/constants.dart';
import 'package:sysponics/model/crop/Crop.dart';
import 'package:sysponics/ui/project/project-page/project-page-crops-historical-data-widget.dart';
import 'package:sysponics/ui/static/empty_list_indicator.dart';

class ProjectPageCropsWidget extends StatefulWidget {
  static const route = '/project-historical-data';

  @override
  State<StatefulWidget> createState() {
    return ProjectPageCropsWidgetState();
  }
}

class ProjectPageCropsWidgetState extends State<ProjectPageCropsWidget> {
  String _projectKey;
  final PagingController<int, Crop> _pagingController = PagingController(firstPageKey: 0);

  @override
  void initState() {
    _pagingController.addPageRequestListener((pageKey) {
      fetchPage(pageKey);
    });
    super.initState();
  }

  void fetchPage(int page) {
    CropDataRetriever().getCropsOfProject(_projectKey).then((list) {
      final isLastPage = list.length < Constants.DEFAULT_PAGE_SIZE;
      if (isLastPage) {
        _pagingController.appendLastPage(list);
      } else {
        _pagingController.appendPage(list, page + 1);
      }
    }).catchError((error) {
      _pagingController.error = error;
    });
  }

  @override
  Widget build(BuildContext context) {
    _projectKey = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: AppBar(
        title: Text('Plantios Passados'),
      ),
      body: RefreshIndicator(
        onRefresh: () => Future.sync(
          () => _pagingController.refresh(),
        ),
        child: PagedListView<int, Crop>(
          pagingController: _pagingController,
          builderDelegate: PagedChildBuilderDelegate<Crop>(
            noItemsFoundIndicatorBuilder: (context) => EmptyListIndicator(),
            itemBuilder: (context, item, index) {
              DateFormat df = DateFormat('dd/MM/yyyy');
              return ListTile(
                leading: Icon(Icons.agriculture),
                title: Text('Plantio feito em ${df.format(item.plantingDate)}'),
                subtitle: Text(item.actualHarvestDate != null
                    ? 'Colhido em ${df.format(item.actualHarvestDate)}'
                    : item.expectedHarvestDate != null
                        ? 'Colheita estimada para ${df.format(item.expectedHarvestDate)}'
                        : 'Não Colhido'),
                onTap: () {
                  Navigator.pushNamed(context, CropHistoricalDataWidget.route, arguments: item.id);
                },
              );
            },
          ),
        ),
      ),
    );
  }
}
