import 'package:date_field/date_field.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:sysponics/connectivity/http2/crop-data-retriever.dart';
import 'package:sysponics/environment/constants.dart';
import 'package:sysponics/model/crop/Crop.dart';
import 'package:sysponics/model/project/Project.dart';

class ProjectHarvestCropPage extends StatefulWidget {
  static const route = '/project-harvest-crop';

  @override
  State<StatefulWidget> createState() {
    return ProjectHarvestCropPageState();
  }
}

class ProjectHarvestCropPageState extends State<ProjectHarvestCropPage> {
  final _key = GlobalKey<FormState>();
  Project _project;
  Crop _crop;
  DateTime _date;
  String _obs;

  @override
  Widget build(BuildContext context) {
    Map<String, dynamic> map = ModalRoute.of(context).settings.arguments;
    _crop = map['crop'] as Crop;
    _project = map['project'] as Project;
    _crop.harvestedSpeciesId = _project.speciesId;
    _crop.harvested = true;
    return Scaffold(
        appBar: AppBar(
          title: Text('Colheita'),
          actions: [
            Builder(builder: (context) {
              return IconButton(
                  icon: Icon(Icons.save),
                  onPressed: () {
                    if (_key.currentState.validate()) {
                      CropDataRetriever().markAsHarvested(_crop.id, _project.speciesId, _date, _obs).then((value) {
                        Navigator.pop(context);
                      }).catchError((error) {
                        Scaffold.of(context).showSnackBar(SnackBar(content: Text(error)));
                      });
                    }
                  });
            })
          ],
        ),
        body: Padding(
          padding: EdgeInsets.symmetric(horizontal: 8),
          child: Form(
            key: _key,
            child: Column(
              children: [
                DateTimeFormField(
                  dateFormat: DateFormat('dd/MM/yyyy'),
                  label: 'Data de Colheita',
                  initialValue: DateTime.now(),
                  firstDate: DateTime(2000),
                  lastDate: DateTime(2100),
                  onDateSelected: (date) {
                    setState(() {
                      _date = date;
                    });
                  },
                  validator: (date) {
                    return date == null ? 'Data de Colheita é obrigatória!' : null;
                  },
                ),
              ],
            ),
          ),
        ));
  }
}
