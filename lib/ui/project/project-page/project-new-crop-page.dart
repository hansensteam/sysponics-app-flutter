import 'package:date_field/date_field.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:sysponics/connectivity/http2/crop-data-retriever.dart';
import 'package:sysponics/connectivity/http2/project-data-retriever.dart';
import 'package:sysponics/connectivity/http2/species-data-retriever.dart';
import 'package:sysponics/dto/speciesdto.dart';
import 'package:sysponics/model/crop/Crop.dart';
import 'package:sysponics/model/project/Project.dart';
import 'package:sysponics/model/species/Species.dart';
import 'package:sysponics/model/species_names/SpeciesNames.dart';
import 'package:sysponics/persistence/dao/species/SpeciesDao.dart';
import 'package:sysponics/persistence/dao/species/SpeciesNamesDao.dart';
import 'package:sysponics/ui/widgets/species-dropdown/species-dropdown.dart';
import 'package:sysponics/ui/widgets/species-dropdown/species-selector.dart';

class ProjectNewCropPage extends StatefulWidget {
  static const route = '/new-crop-page';

  @override
  State<StatefulWidget> createState() {
    return ProjectNewCropPageState();
  }
}

class ProjectNewCropPageState extends State<ProjectNewCropPage> {
  final _formKey = GlobalKey<FormState>();
  Crop _crop = Crop(plantingDate: DateTime.now(), harvested: false);
  SpeciesDTO dto;
  Project _project;

  @override
  Widget build(BuildContext context) {
    _project = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: AppBar(
        title: Text('Novo plantio'),
        actions: [
          IconButton(
              icon: Icon(Icons.save),
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  ProjectDataRetriever().save(_project).then((p) {
                    CropDataRetriever().saveCrop(_crop, _project.id).then((c) {
                      Navigator.pop(context);
                    });
                  });
                }
              })
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              DateTimeFormField(
                dateFormat: DateFormat('dd/MM/yyyy'),
                label: 'Data de Plantio',
                initialValue: DateTime.now(),
                firstDate: DateTime(2000),
                lastDate: DateTime(2100),
                onDateSelected: (date) {
                  setState(() {
                    _crop.plantingDate = date;
                  });
                },
                validator: (date) {
                  return date == null ? 'Data de Plantio é obrigatória!' : null;
                },
              ),
              DateTimeFormField(
                dateFormat: DateFormat('dd/MM/yyyy'),
                label: 'Colheita estimada para',
                initialValue: DateTime.now(),
                firstDate: DateTime(2020),
                lastDate: DateTime(2100),
                onDateSelected: (date) {
                  setState(() {
                    _crop.expectedHarvestDate = date;
                  });
                },
                validator: (date) {
                  return date == null ? 'Estimativa de Colheita é obrigatória!' : null;
                },
              ),
              FutureBuilder(
                future: SpeciesDataRetriever().speciesOfLocale(),
                builder: (BuildContext context, AsyncSnapshot<List<SpeciesDTO>> snapshot) {
                  if (!snapshot.hasData) {
                    return CircularProgressIndicator();
                  }
                  return DropdownButton<int>(
                    isExpanded: true,
                    value: _project.speciesId,
                    hint: Text('Espécie'),
                    items: snapshot.data.map((species) {
                      return DropdownMenuItem<int>(
                        value: species.id,
                        child: Text(species.commonName + ' (' + species.cientificName + ')'),
                      );
                    }).toList(),
                    onChanged: (species) {
                      setState(() {
                        _project.speciesId = species;
                      });
                    },
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
