import 'package:flutter/material.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:sysponics/assets/CustomColors.dart';
import 'package:sysponics/connectivity/http2/historical-data-retriever.dart';
import 'package:sysponics/environment/constants.dart';
import 'package:sysponics/model/data/HistoricalData.dart';
import 'package:intl/intl.dart';
import 'package:sysponics/ui/static/empty_list_indicator.dart';

class CropHistoricalDataWidget extends StatefulWidget {
  static const route = '/cropHistoricalDataWidget';

  @override
  State<StatefulWidget> createState() {
    return CropHistoricalDataWidgetState();
  }
}

class CropHistoricalDataWidgetState extends State<CropHistoricalDataWidget> {
  int _cropId;
  final PagingController<int, HistoricalData> _pagingController = PagingController(firstPageKey: 0);

  @override
  void initState() {
    _pagingController.addPageRequestListener((pageKey) {
      fetchPage(pageKey);
    });
    super.initState();
  }

  void fetchPage(int page) {
    HistoricalDataRetriever().findAllByCropId(_cropId, page).then((value) {
      final isLastPage = (value.length < Constants.DEFAULT_PAGE_SIZE);
      if (isLastPage) {
        _pagingController.appendLastPage(value);
      } else {
        final nextPageKey = page + 1;
        _pagingController.appendPage(value, nextPageKey);
      }
    }).catchError((error) {
      print(error);
      _pagingController.error = error;
    });
  }

  @override
  Widget build(BuildContext context) {
    _cropId = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: AppBar(
        title: Text('Histórico de Medições'),
      ),
      body: RefreshIndicator(
        onRefresh: () => Future.sync(
          () => _pagingController.refresh(),
        ),
        child: PagedListView<int, HistoricalData>(
          pagingController: _pagingController,
          builderDelegate: PagedChildBuilderDelegate<HistoricalData>(
            itemBuilder: (context, item, index) {
              DateFormat df = DateFormat('dd/MM/yyyy HH:mm:ss');
              return ListTile(
                leading: Icon(Constants.IC_HISTORY),
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('${item.type.description}'),
                    value(item),
                  ],
                ),
                subtitle: Text('${df.format(item.timestamp)}'),
              );
            },
            noItemsFoundIndicatorBuilder: (context) => EmptyListIndicator(),
          ),
        ),
      ),
    );
  }

  Widget value(HistoricalData data) {
    switch (data.type) {
      case HistoricalDataType.WATER_TEMP:
      case HistoricalDataType.AIR_TEMP:
        return Text('${data.value} ºC');
      case HistoricalDataType.WATER_PH:
        return Text('${data.value}');
        break;
      case HistoricalDataType.AIR_HUM:
        return Text('${data.value} %');
      case HistoricalDataType.WATER_LEVEL:
        return Icon(
          data.value == 0 ? Constants.IC_DEVICE_ON : Constants.IC_DEVICE_OFF,
          color: data.value == 0 ? CustomColors.APP_BRAND_COLOR : Colors.redAccent,
        );
      case HistoricalDataType.LIGHTS:
      case HistoricalDataType.WATER_PUMP:
        return Icon(
          data.value == 1 ? Constants.IC_DEVICE_ON : Constants.IC_DEVICE_OFF,
          color: data.value == 1 ? CustomColors.APP_BRAND_COLOR : CustomColors.DISABLED,
        );
    }
  }

  @override
  void dispose() {
    _pagingController.dispose();
    super.dispose();
  }
}
