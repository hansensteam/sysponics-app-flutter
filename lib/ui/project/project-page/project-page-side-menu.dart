import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sysponics/connectivity/http2/crop-data-retriever.dart';
import 'package:sysponics/environment/constants.dart';
import 'package:sysponics/model/crop/Crop.dart';
import 'package:sysponics/model/project/Project.dart';
import 'file:///E:/PROJECTS/Sysponics/sysponics_app_flutter/lib/ui/project/project-page/project-page-crops-widget.dart';
import 'package:sysponics/ui/project/project-page/project-settings.dart';

class ProjectSideMenu extends StatefulWidget {
  final Project project;

  ProjectSideMenu({this.project});

  @override
  State<StatefulWidget> createState() {
    return ProjectPageSideMenuState(project: this.project);
  }
}

class ProjectPageSideMenuState extends State<ProjectSideMenu> {
  Project project;
  bool showSettings = false;

  ProjectPageSideMenuState({this.project});

  @override
  void initState() {
    CropDataRetriever().getActiveCrop(project.key).then((value) => setState(() {
          showSettings = value != null;
        }));
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          ListTile(
            leading: Icon(Constants.IC_HISTORY),
            title: Text('Histórico'),
            onTap: () {
              Navigator.popAndPushNamed(context, ProjectPageCropsWidget.route, arguments: project.key);
            },
          ),
          if (showSettings)
            ListTile(
              leading: Icon(Icons.settings),
              title: Text('Configurações'),
              onTap: () {
                Navigator.popAndPushNamed(context, ProjectSettingsPage.route, arguments: project);
              },
            ),
        ],
      ),
    );
  }
}
