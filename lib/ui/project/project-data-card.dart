import 'package:flutter/material.dart';
import 'package:sysponics/assets/CustomColors.dart';
import 'package:sysponics/connectivity/http2/crop-data-retriever.dart';
import 'package:sysponics/connectivity/http2/species-data-retriever.dart';
import 'package:sysponics/dto/speciesdto.dart';
import 'package:sysponics/environment/constants.dart';
import 'package:sysponics/model/crop/Crop.dart';
import 'package:sysponics/model/dto/crop-last-data/crop-last-data.dart';
import 'package:sysponics/model/project/Project.dart';

class ProjectDataCard extends StatefulWidget {
  final Project project;

  const ProjectDataCard({Key key, this.project}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return ProjectDataCardState(project);
  }
}

class ProjectDataCardState extends State<ProjectDataCard> {
  final Project project;

  ProjectDataCardState(this.project);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Row(
              children: [
                CircleAvatar(
                  child: Icon(Constants.IC_PROJECT),
                  backgroundColor: CustomColors.APP_BRAND_COLOR,
                  foregroundColor: CustomColors.APP_BACKGROUND,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 8),
                  child: Text(
                    project.name,
                    style: TextStyle(fontSize: 20),
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Row(
                children: [
                  FutureBuilder(
                    future: SpeciesDataRetriever().speciesOfLocale(),
                    builder: (BuildContext context, AsyncSnapshot<List<SpeciesDTO>> snapshot) {
                      Widget w = Text('Espécie não definida');
                      if (snapshot.hasData) {
                        for (SpeciesDTO dto in snapshot.data) {
                          if (dto.id == project.speciesId) {
                            w = Text('Espécie: ${dto.commonName} (${dto.cientificName})');
                          }
                        }
                      }
                      return w;
                    },
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: buildCrops(context, project),
            )
          ],
        ),
      ),
    );
  }

  FutureBuilder<Crop> buildCrops(BuildContext context, Project project) {
    return FutureBuilder(
      future: CropDataRetriever().getActiveCrop(project.key),
      builder: (BuildContext context, AsyncSnapshot<Crop> snapshot) {
        if (snapshot.hasData) {
          return historicalData(context, snapshot.data);
        } else {
          return Text('Sem plantio ativo');
        }
      },
    );
  }

  FutureBuilder<CropLastDataDTO> historicalData(BuildContext context, Crop crop) {
    return FutureBuilder(
      future: CropDataRetriever().getLastData(crop.id),
      builder: (BuildContext context, AsyncSnapshot<CropLastDataDTO> snapshot) {
        if(!snapshot.hasData){
          return CircularProgressIndicator();
        }
        CropLastDataDTO dto = snapshot.data;
        return Row(
          children: [
            Column(
              children: [
                data(Constants.IC_AIR_TEMPERATURE, value: dto.airTemp),
                data(Constants.IC_WATER_PUMP, value: dto.waterPh),
              ],
            ),
            Column(
              children: [
                data(Constants.IC_AIR_HUMIDITY, value: dto.airHum),
                data(Constants.IC_WATER_TEMPERATURE, value: dto.waterTemp),
              ],
            ),
            Column(
              children: [
                data(Constants.IC_LIGHTS, onOff: dto.lightsOn, isNumeric: false),
                data(Constants.IC_WATER_PUMP, onOff: dto.pumpOn, isNumeric: false),
              ],
            ),
            Column(
              children: [
              ],
            )
          ],
        );
      },
    );
  }

  Widget data(IconData icon, {double value = -1, bool onOff = false, bool inverColors = false, bool isNumeric = true}) {
    if (isNumeric) {
      return FlatButton(
        onPressed: () {},
        child: Row(
          children: [
            Icon(icon),
            Text('$value'),
          ],
        ),
      );
    } else {
      return FlatButton(
        onPressed: () {},
        child: Row(
          children: [
            Icon(icon),
            Icon(
              onOff ? Constants.IC_DEVICE_ON : Constants.IC_DEVICE_OFF,
              color: inverColors ? (onOff ? CustomColors.DISABLED : CustomColors.APP_BRAND_COLOR) : (onOff ? CustomColors.APP_BRAND_COLOR : CustomColors.DISABLED),
            ),
          ],
        ),
      );
      // Icon(
      //   dto.pumpOn ? Constants.IC_DEVICE_ON : Constants.IC_DEVICE_OFF,
      //   color: dto.pumpOn ? CustomColors.APP_BRAND_COLOR : CustomColors.DISABLED,
      // )
    }
  }
}
