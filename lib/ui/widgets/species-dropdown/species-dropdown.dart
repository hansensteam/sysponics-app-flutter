import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sysponics/connectivity/http2/species-data-retriever.dart';
import 'package:sysponics/dto/speciesdto.dart';
import 'package:sysponics/model/species/Species.dart';

typedef SpeciesCallback = void Function(SpeciesDTO);

class SpeciesDropDown extends StatefulWidget {
  final SpeciesCallback onChange;

  const SpeciesDropDown({Key key, this.onChange}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return SpeciesDropDownState(onChange);
  }
}

class SpeciesDropDownState extends State<SpeciesDropDown> {
  final SpeciesCallback doOnChange;

  SpeciesDTO value;
  List<DropdownMenuItem<SpeciesDTO>> _items = List.empty(growable: true);

  SpeciesDropDownState(this.doOnChange);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        FutureBuilder(
          future: SpeciesDataRetriever().speciesOfLocale(),
          builder: (BuildContext context, AsyncSnapshot<List<SpeciesDTO>> snapshot) {
              this._items = List.empty(growable: true);
            switch (snapshot.connectionState) {
              case ConnectionState.waiting:
                return CircularProgressIndicator();
              default:
                if (snapshot.hasData) {
                  for (SpeciesDTO dto in snapshot.data) {
                    this._items.add(
                        DropdownMenuItem<SpeciesDTO>(
                          value: dto,
                          child: Text(dto.commonName.isNotEmpty ? dto.commonName : dto.cientificName),
                        )
                    );
                  }
                  return DropdownButton<SpeciesDTO>(
                    value: this.value,
                    items: _items,
                    onChanged: (dto) {
                      value = dto;
                      setState(() {
                        if (doOnChange != null) {
                          doOnChange(dto);
                        }
                      });
                    },
                  );
                } else {
                  return Text('Erro carregando especies');
                }
            }
          },
        ),
        IconButton(
          icon: Icon(Icons.add),
          onPressed: () {},
        ),
      ],
    );
  }
}
