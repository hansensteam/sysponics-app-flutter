import 'package:flutter/material.dart';
import 'package:sysponics/connectivity/http2/species-data-retriever.dart';
import 'package:sysponics/dto/speciesdto.dart';

typedef OnData = Widget Function(List<SpeciesDTO>);
typedef OnNoData = Widget Function();
typedef OnLoading = Widget Function();

class SpeciesSelector extends StatefulWidget {
  final OnData onData;
  final OnNoData onNoData;
  final OnLoading onLoading;

  SpeciesSelector({this.onData, this.onNoData, this.onLoading});

  @override
  State<StatefulWidget> createState() {
    return SpeciesSelectorState(onData, onNoData, onLoading);
  }
}

class SpeciesSelectorState extends State<SpeciesSelector> {
  final OnData onData;
  final OnNoData onNoData;
  final OnLoading onLoading;

  SpeciesSelectorState(this.onData, this.onNoData, this.onLoading);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          FutureBuilder(
            future: SpeciesDataRetriever().speciesOfLocale(),
            builder: (BuildContext context, AsyncSnapshot<List<SpeciesDTO>> snapshot) {
              switch (snapshot.connectionState) {
                case ConnectionState.waiting:
                  return onLoading();
                default:
                  if (snapshot.hasData) {
                    return onData(snapshot.data);
                  } else {
                    return onNoData();
                  }
              }
            },
          ),
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () {},
          ),
        ],
      ),
    );
  }
}
