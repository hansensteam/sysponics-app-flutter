import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sysponics/assets/CustomColors.dart';
import 'package:sysponics/environment/constants.dart';
import 'package:sysponics/ui/devices/device-list-form.dart';
import 'package:sysponics/ui/login/login-form.dart';
import 'package:sysponics/ui/project/project-list-form.dart';
import 'package:sysponics/ui/species/species-list.dart';

class SideMenu extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SideMenuState();
  }
}

class SideMenuState extends State<SideMenu> {
  String _name;

  @override
  void initState() {
    super.initState();
  }

  getData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Map<String, dynamic> userData = jsonDecode(prefs.get(Constants.PREFS_USER_DATA));
    _name = userData['nome'];
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: FutureBuilder<SharedPreferences>(
          future: SharedPreferences.getInstance(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return CircularProgressIndicator();
            } else {
              return ListView(
                padding: EdgeInsets.zero,
                children: [
                  DrawerHeader(
                    child: Column(
                      children: [
                        Align(
                          alignment: Alignment.topLeft,
                          child: Icon(
                            Icons.account_circle,
                            size: 96.0,
                            color: CustomColors.APP_BACKGROUND,
                            textDirection: TextDirection.ltr,
                          ),
                        ),
                        Align(
                          alignment: Alignment.bottomLeft,
                          child: Text(
                            jsonDecode(snapshot.data.get(Constants.PREFS_USER_DATA))['nome'],
                            style: TextStyle(
                              color: CustomColors.APP_BACKGROUND,
                              fontSize: 22,
                            ),
                          ),
                        ),
                      ],
                    ),
                    decoration: BoxDecoration(color: CustomColors.APP_BRAND_COLOR),
                  ),
                  ListTile(
                    title: Text('Início'),
                    onTap: () {
                      Navigator.popAndPushNamed(context, ProjectListForm.route);
                    },
                  ),
                  ListTile(
                    title: Text('Dispositivos'),
                    onTap: () {
                      Navigator.pushNamed(context, DeviceListForm.route);
                    },
                  ),
                  ListTile(
                    title: Text('Espécies'),
                    onTap: () {
                      Navigator.pushNamed(context, SpeciesList.route);
                    },
                  ),
                  ListTile(
                    title: Text('Configurações'),
                    onTap: () {},
                  ),
                  ListTile(
                    title: Text('Sair'),
                    onTap: () {
                      logout();
                    },
                  )
                ],
              );
            }
          }),
    );
  }

  void logout() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    await sp.clear();
    Navigator.popUntil(context, (route) {
      return Navigator.canPop(context);
    });
    Navigator.pushNamed(context, LoginForm.route);
  }
}
