import 'package:flutter/material.dart';
import 'package:sysponics/assets/CustomColors.dart';

class CustomCircularProgressIndicator extends CircularProgressIndicator {
  CustomCircularProgressIndicator()
      : super(
          backgroundColor: CustomColors.APP_BACKGROUND,
          valueColor: AlwaysStoppedAnimation<Color>(CustomColors.APP_BRAND_COLOR),
        );
}
