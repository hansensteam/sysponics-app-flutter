import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:sysponics/assets/CustomColors.dart';
import 'package:sysponics/connectivity/http2/species-data-retriever.dart';
import 'package:sysponics/dto/speciesdto.dart';
import 'package:sysponics/environment/constants.dart';
import 'package:sysponics/ui/species/species-form.dart';
import 'package:sysponics/ui/widgets/side-menu/side-menu.dart';

class SpeciesList extends StatefulWidget {
  static const route = '/species-list';

  @override
  State<StatefulWidget> createState() {
    return SpeciesListState();
  }
}

class SpeciesListState extends State<SpeciesList> {
  final PagingController<int, SpeciesDTO> _pagingController = PagingController(firstPageKey: 0);

  @override
  void initState() {
    _pagingController.addPageRequestListener((pageKey) {
      fetchPage(pageKey);
    });
    super.initState();
  }

  void fetchPage(int p) {
    SpeciesDataRetriever().speciesOfLocale(page: p).then((list) {
      final isLastPage = list.length < Constants.DEFAULT_PAGE_SIZE;
      //Se for a última página, chama o appendLastPage, senão chama o appendPage
      if (isLastPage) {
        _pagingController.appendLastPage(list);
      } else {
        _pagingController.appendPage(list, p + 1);
      }
    }).catchError((error) {
      _pagingController.error = error;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.APP_BACKGROUND,
      appBar: AppBar(
        title: Text('Espécies'),
      ),
      drawer: SideMenu(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        backgroundColor: CustomColors.APP_BRAND_COLOR,
        foregroundColor: CustomColors.APP_BACKGROUND,
        onPressed: () {
          Navigator.pushNamed(context, SpeciesForm.route).then((value) {
            _pagingController.refresh();
          });
        },
      ),
      body: RefreshIndicator(
        onRefresh: () => Future.sync(
          () => _pagingController.refresh(),
        ),
        child: PagedListView<int, SpeciesDTO>(
          pagingController: _pagingController,
          builderDelegate: PagedChildBuilderDelegate<SpeciesDTO>(
            itemBuilder: (context, item, index) {
              return Slidable(
                actionPane: SlidableDrawerActionPane(),
                actionExtentRatio: 0.25,
                child: ListTile(
                  leading: CircleAvatar(
                    backgroundColor: CustomColors.APP_BRAND_COLOR,
                    foregroundColor: CustomColors.APP_BACKGROUND,
                    child: Icon(Constants.IC_SPECIES),
                  ),
                  title: Text('${item.commonName} (${item.cientificName})'),
                ),
                secondaryActions: [
                  IconSlideAction(
                    color: Colors.blue,
                    caption: 'Editar',
                    icon: Icons.edit,
                    onTap: () {
                      SpeciesDataRetriever().findById(item.id).then((s) {
                        Navigator.pushNamed(context, SpeciesForm.route, arguments: s).then((value) => _pagingController.refresh());
                      }).catchError((error) {
                        Scaffold.of(context).showSnackBar(
                          SnackBar(
                            content: Text(error),
                          ),
                        );
                      });
                    },
                  ),
                  IconSlideAction(
                    color: Colors.red,
                    caption: 'Excluir',
                    icon: Icons.delete,
                    onTap: () {
                      deleteClick(item);
                    },
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }

  deleteClick(SpeciesDTO species) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
              title: Text('Confirme'),
              actions: [
                TextButton(
                    child: Text('Sim'),
                    onPressed: () {
                      Navigator.of(context).pop();
                      SpeciesDataRetriever().deleteById(species.id).then((b) {
                        _pagingController.refresh();
                      }).catchError((error) {
                        Scaffold.of(context).showSnackBar(
                          SnackBar(
                            content: Text(error),
                          ),
                        );
                      });
                    }),
                TextButton(
                  child: Text('Não'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
              content: SingleChildScrollView(
                child: ListBody(
                  children: [
                    Text('Deseja realmente excluir a Espécie ${species.commonName} (${species.cientificName})?'),
                  ],
                ),
              ));
        });
  }


}
