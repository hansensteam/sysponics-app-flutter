import 'package:flutter/material.dart';
import 'package:sysponics/assets/CustomColors.dart';
import 'package:sysponics/connectivity/http2/species-data-retriever.dart';
import 'package:sysponics/model/species/Species.dart';
import 'package:sysponics/model/species_names/SpeciesNames.dart';
import 'package:sysponics/ui/login/login-form.dart';

class SpeciesForm extends StatefulWidget {
  static const route = '/species-form';

  @override
  State<StatefulWidget> createState() {
    return SpeciesFormState();
  }
}

class SpeciesFormState extends State<SpeciesForm> {
  final _formKey = GlobalKey<FormState>();
  Species _species = Species(commonNames: List.empty(growable: true));
  List<SpeciesNames> _names = List.empty(growable: true);

  final TextEditingController _cientificNameFilter = TextEditingController();
  final TextEditingController _commonNameFilter = TextEditingController();

  @override
  void initState() {
    addListeners();
  }

  void addListeners() {
    _cientificNameFilter.addListener(() {
      _species.cientificName = _cientificNameFilter.text.isEmpty ? '' : _cientificNameFilter.text;
    });
    _commonNameFilter.addListener(() {
      if (_names.length == 0) {
        _names.add(SpeciesNames(locale: 'pt_br', commonName: _commonNameFilter.text.isEmpty ? '' : _commonNameFilter.text));
      } else {
        _names[0].commonName = _commonNameFilter.text.isEmpty ? '' : _commonNameFilter.text;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (ModalRoute.of(context).settings.arguments != null) {
      _species = ModalRoute.of(context).settings.arguments;
      _cientificNameFilter.text = _species.cientificName;
      _names = _species.commonNames;
    }
    return Scaffold(
      backgroundColor: CustomColors.APP_BACKGROUND,
      appBar: AppBar(
        title: Text('Espécie'),
        actions: [
          IconButton(
            icon: Icon(Icons.save),
            onPressed: () {
              if (_formKey.currentState.validate()) {
                _species.commonNames = _names;
                SpeciesDataRetriever().save(_species).then((value) {
                  Navigator.pop(context);
                }).catchError((error) {
                  Scaffold.of(context).showSnackBar(
                    SnackBar(
                      content: Text(error),
                    ),
                  );
                });
              }
            },
          )
        ],
      ),
      body: Form(
        key: _formKey,

        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10.0),
          child: Column(
            children: [
              TextFormField(
                controller: _cientificNameFilter,
                decoration: InputDecoration(labelText: 'Nome Científico'),
                validator: (text) {
                  return text.isEmpty ? 'O Nome Científico é Obrigatório!' : null;
                },
              ),
              TextFormField(
                controller: _commonNameFilter,
                decoration: InputDecoration(labelText: 'Nome Comum'),
                validator: (text) {
                  return text.isEmpty ? 'O Nome Comum é Obrigatório!' : null;
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}


