import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sysponics/assets/CustomColors.dart';
import 'package:sysponics/environment/constants.dart';
import 'package:sysponics/model/login-data/LoginData.dart';
import 'package:sysponics/model/login-data/logged-user.dart';
import 'package:sysponics/ui/project/project-list-form.dart';

// Create a Form widget.
class LoginForm extends StatefulWidget {
  static const route = '/login-form';

  @override
  _LoginFormState createState() {
    return _LoginFormState();
  }
}

enum FormType { login, registro }

class _LoginFormState extends State<LoginForm> {
  final TextEditingController _emailFilter = TextEditingController();
  final TextEditingController _passwordFilter = TextEditingController();
  final TextEditingController _nameFilter = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  FormType _formType = FormType.login;
  String _email;
  String _password;
  String _name;

  _LoginFormState() {
    _emailFilter.addListener(_emailListen);
    _passwordFilter.addListener(_passwordListen);
    _nameFilter.addListener(_nameListen);
  }

  void _emailListen() {
    if (_emailFilter.text.isEmpty) {
      _email = '';
    } else {
      _email = _emailFilter.text;
    }
  }

  void _passwordListen() {
    if (_passwordFilter.text.isEmpty) {
      _password = '';
    } else {
      _password = _passwordFilter.text;
    }
  }

  void _nameListen() {
    if (_nameFilter.text.isEmpty) {
      _name = '';
    } else {
      _name = _nameFilter.text;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.APP_BACKGROUND,
      appBar: AppBar(
        backgroundColor: CustomColors.APP_BRAND_COLOR,
        title: Text(
          'Sysponics',
          style: TextStyle(color: CustomColors.APP_BACKGROUND),
        ),
      ),
      body: Form(
        key: _formKey,
        child: ListView(
          children: <Widget>[_buildTextFields(), _buildButtons()],
        ),
      ),
    );
    // Build a Form widget using the _formKey created above.
  }

  List<Widget> _textFields() {
    List<Widget> list = new List();
    if (_formType != FormType.login) {
      list.add(TextFormField(
        controller: _nameFilter,
        decoration: InputDecoration(labelText: "Nome"),
        validator: (value) {
          return value.isEmpty ? "Informe o nome" : null;
        },
      ));
    }
    list.add(TextFormField(
      controller: _emailFilter,
      decoration: InputDecoration(labelText: "E-mail"),
      autofocus: _formType == FormType.login,
      keyboardType: TextInputType.emailAddress,
      validator: (value) {
        return value.isEmpty ? "Informe o e-mail!" : null;
      },
    ));
    list.add(TextFormField(
      controller: _passwordFilter,
      decoration: InputDecoration(labelText: "Senha"),
      obscureText: true,
      validator: (value) {
        return value.isEmpty ? "Informe a senha!" : null;
      },
    ));
    return list;
  }

  Widget _buildTextFields() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: _textFields(),
      ),
    );
  }

  List<Widget> _buttons() {
    List<Widget> list = List();
    list.add(Builder(builder: (BuildContext context) {
      return RaisedButton(
        child: Text(_formType == FormType.login ? "Login" : "Criar Conta"),
        onPressed: () => _formType == FormType.login ? _loginPressed(context) : _createAccountPressed(context),
      );
    }));
    list.add(FlatButton(
      child: Text(_formType == FormType.login ? "Não possui conta? Registre-se" : "Já possui conta? Faça o Login"),
      onPressed: _formChange,
    ));
    if (_formType == FormType.login) {
      list.add(FlatButton(
        child: Text("Esqueci a senha"),
        onPressed: _passwordReset,
      ));
    }
    return list;
  }

  Widget _buildButtons() {
    return Column(
      children: _buttons(),
    );
  }

  Future<LoginData> doLogin(email, password) async {
    print('calling request');
    final response = await http.post('${Constants.BASE_URL}/oauth/token', headers: {'Content-Type': 'application/x-www-form-urlencoded', 'Authorization': 'Basic ${Constants.OAUTH_BASIC}'}, body: {'username': email, 'password': password, 'grant_type': 'password'});

    if (response.statusCode == 200) {
      return LoginData.fromJson(json.decode(response.body));
    } else {
      throw Exception('${response.statusCode}  - Login Failed: ${response.body}');
    }
  }

  Future<LoginData> doRegister(name, email, password) async {
    final response = await http.post(
      Constants.BASE_URL + '/user/register',
      headers: {'Content-Type': 'application/json'},
      body: {json.encode('{"nome":"$name", "email":"$email", "password":"$password"}')},
    );
    if (response.statusCode == 200) {
      return LoginData.fromJson(json.decode(response.body));
    } else {
      throw Exception('${response.statusCode} - Register failed: ${response.body}');
    }
  }

  void _loginPressed(BuildContext context) {
    // Validate returns true if the form is valid, or false
    // otherwise.
    if (_formKey.currentState.validate()) {
      doLogin(_email, _password).then((ld) {
        http.get(Constants.BASE_URL + '/user/user-info', headers: {'Authorization': 'Bearer ${ld.accessToken}'}).then((userInfo) {
          Map<String, dynamic> data = jsonDecode(userInfo.body);
          LoggedUser user = LoggedUser.fromJson(data['principal']);
          _loginSuccess(ld, user);
        }).catchError((error) {
          print(error);
          Scaffold.of(context).showSnackBar(SnackBar(content: Text(error)));
        });
      }).catchError((e) {
        _loginError(e);
      });
    }
    new CircularProgressIndicator();
  }

  void _loginSuccess(LoginData ld, LoggedUser user) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(Constants.PREFS_ACCESS_TOKEN, ld.accessToken);
    prefs.setString(Constants.PREFS_REFRESH_TOKEN, ld.refreshToken);
    prefs.setString(Constants.PREFS_USER_DATA, jsonEncode(user.toMap()));
    Navigator.popAndPushNamed(context, ProjectListForm.route);
  }

  void _loginError(e) {
    print(e);
  }

  void _formChange() {
    setState(() {
      if (_formType == FormType.login) {
        _formType = FormType.registro;
      } else {
        _formType = FormType.login;
      }
    });
  }

  void _passwordReset() {}

  void _createAccountPressed( BuildContext context ) {
    if (_formKey.currentState.validate()) {
      new CircularProgressIndicator();
      doRegister(_name, _email, _password).then((value) => setState(() {})).catchError((error) {
        Scaffold.of(context).showSnackBar(SnackBar(content: Text(error)));
      });
    }
  }
}
