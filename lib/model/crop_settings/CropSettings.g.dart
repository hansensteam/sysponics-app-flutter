// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'CropSettings.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CropSettings _$CropSettingsFromJson(Map<String, dynamic> json) {
  return CropSettings(
    id: json['id'],
    key: json['key'],
    cropId: json['crop_id'] as int,
    waterPh: (json['waterPH'] as num)?.toDouble(),
    millisToVerify: json['millisToVerify'] as int,
  );
}

Map<String, dynamic> _$CropSettingsToJson(CropSettings instance) =>
    <String, dynamic>{
      'id': instance.id,
      'key': instance.key,
      'crop_id': instance.cropId,
      'waterPH': instance.waterPh,
      'millisToVerify': instance.millisToVerify,
    };
