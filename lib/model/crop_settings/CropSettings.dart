import 'package:json_annotation/json_annotation.dart';
import 'package:sysponics/model/GenericModel.dart';
import 'package:sysponics/model/crop_schedules/CropSchedules.dart';

part 'CropSettings.g.dart';

@JsonSerializable(disallowUnrecognizedKeys: false, ignoreUnannotated: true)
class CropSettings extends GenericModel {
  @JsonKey(name: "crop_id")
  int cropId;
  @JsonKey(name: "waterPH")
  double waterPh;
  @JsonKey(name: "millisToVerify")
  int millisToVerify;

  List<CropSchedules> schedules = List.empty(growable: true);

  CropSettings({id, key, this.cropId, this.waterPh, this.millisToVerify}) : super(id, key);

  @override
  Map<String, dynamic> toMap() => _$CropSettingsToJson(this);

  factory CropSettings.fromJson(Map<String, dynamic> json) => _$CropSettingsFromJson(json);
}
