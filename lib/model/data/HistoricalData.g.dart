// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'HistoricalData.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HistoricalData _$HistoricalDataFromJson(Map<String, dynamic> json) {
  return HistoricalData(
    id: json['id'],
    key: json['key'],
    cropId: json['crop_id'] as int,
    deviceId: json['device_id'] as int,
    timestamp: json['timestamp'] == null
        ? null
        : DateTime.parse(json['timestamp'] as String),
    value: (json['value'] as num)?.toDouble(),
    type: _$enumDecodeNullable(_$HistoricalDataTypeEnumMap, json['type']),
  );
}

Map<String, dynamic> _$HistoricalDataToJson(HistoricalData instance) =>
    <String, dynamic>{
      'id': instance.id,
      'key': instance.key,
      'crop_id': instance.cropId,
      'device_id': instance.deviceId,
      'timestamp': instance.timestamp?.toIso8601String(),
      'value': instance.value,
      'type': _$HistoricalDataTypeEnumMap[instance.type],
    };

T _$enumDecode<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }

  final value = enumValues.entries
      .singleWhere((e) => e.value == source, orElse: () => null)
      ?.key;

  if (value == null && unknownValue == null) {
    throw ArgumentError('`$source` is not one of the supported values: '
        '${enumValues.values.join(', ')}');
  }
  return value ?? unknownValue;
}

T _$enumDecodeNullable<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source, unknownValue: unknownValue);
}

const _$HistoricalDataTypeEnumMap = {
  HistoricalDataType.WATER_TEMP: 'WATER_TEMP',
  HistoricalDataType.WATER_PH: 'WATER_PH',
  HistoricalDataType.AIR_TEMP: 'AIR_TEMP',
  HistoricalDataType.AIR_HUM: 'AIR_HUM',
  HistoricalDataType.WATER_LEVEL: 'WATER_LEVEL',
  HistoricalDataType.LIGHTS: 'LIGHTS',
  HistoricalDataType.WATER_PUMP: 'WATER_PUMP',
};
