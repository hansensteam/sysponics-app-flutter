import 'package:json_annotation/json_annotation.dart';
import 'package:sysponics/model/GenericModel.dart';
import 'package:intl/intl.dart';

part 'HistoricalData.g.dart';

@JsonSerializable(ignoreUnannotated: true, disallowUnrecognizedKeys: false)
class HistoricalData extends GenericModel {
  @JsonKey(name: "crop_id")
  int cropId;
  @JsonKey(name: "device_id")
  int deviceId;
  @JsonKey(name: "timestamp")
  DateTime timestamp;
  @JsonKey(name: "value")
  double value;
  @JsonKey(name: "type")
  HistoricalDataType type;

  HistoricalData({id, key, this.cropId, this.deviceId, this.timestamp, this.value, this.type}) : super(id, key);

  @override
  Map<String, dynamic> toMap() {
    DateFormat df = DateFormat('dd/MM/yyyy HH:mm:ss');
    return <String, dynamic>{
      'id': this.id,
      'key': this.key,
      'crop_id': this.cropId,
      'device_id': this.deviceId,
      'timestamp': this.timestamp != null ? df.format(this.timestamp) : null,
      'value': this.value,
      'type': _$HistoricalDataTypeEnumMap[this.type],
    };
  }

  factory HistoricalData.fromJson(Map<String, dynamic> json) {
    DateFormat df = DateFormat('dd/MM/yyyy HH:mm:ss');
    return HistoricalData(
      id: json['id'],
      key: json['key'],
      cropId: json['crop_id'] as int,
      deviceId: json['device_id'] as int,
      timestamp: json['timestamp'] == null ? null : df.parse(json['timestamp'] as String),
      value: (json['value'] as num)?.toDouble(),
      type: _$enumDecodeNullable(_$HistoricalDataTypeEnumMap, json['type']),
    );
  }
}

enum HistoricalDataType { WATER_TEMP, WATER_PH, AIR_TEMP, AIR_HUM, WATER_LEVEL, LIGHTS, WATER_PUMP }

extension HistoricalDataTypeExtension on HistoricalDataType {
  String get description {
    switch (this) {
      case HistoricalDataType.WATER_TEMP:
        return 'Temperatura da Água';
      case HistoricalDataType.WATER_PH:
        return 'PH da Água';
      case HistoricalDataType.AIR_TEMP:
        return 'Temperatura do Ar';
      case HistoricalDataType.AIR_HUM:
        return 'Umidade do Ar';
      case HistoricalDataType.WATER_LEVEL:
        return 'Nível da Água';
      case HistoricalDataType.LIGHTS:
        return 'Iluminação';
      case HistoricalDataType.WATER_PUMP:
        return 'Bomba d\'água';
    }
  }
}
