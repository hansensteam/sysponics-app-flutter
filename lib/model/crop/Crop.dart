import 'package:date_field/date_field.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:sysponics/model/GenericModel.dart';
import 'package:intl/intl.dart';

part 'Crop.g.dart';

@JsonSerializable(disallowUnrecognizedKeys: false, ignoreUnannotated: true)
class Crop extends GenericModel {
  @JsonKey(name: "plantingDate")
  DateTime plantingDate;
  @JsonKey(name: "expectedHarvestDate")
  DateTime expectedHarvestDate;
  @JsonKey(name: "actualHarvestDate")
  DateTime actualHarvestDate;
  @JsonKey(name: "harvested", fromJson: _toBool)
  bool harvested;
  @JsonKey(name: "settings_id")
  int settingsId;
  @JsonKey(name: 'observacoes')
  String observacoes;
  @JsonKey(name: 'harvestedSpeciesId')
  int harvestedSpeciesId;

  static _toBool(value) {
    if (value == null) {
      return false;
    }
    if (value is String) {
      return value.toLowerCase() == "true" ? true : false;
    }
    if (value is int) {
      return value == 1;
    }
  }

  Crop({
    id,
    key,
    this.plantingDate,
    this.expectedHarvestDate,
    this.actualHarvestDate,
    this.harvested,
    this.settingsId,
    this.observacoes,
    this.harvestedSpeciesId,
  }) : super(id, key);

  @override
  Map<String, dynamic> toMap() {
    DateFormat df = DateFormat('dd/MM/yyyy');

    return <String, dynamic>{
      'id': this.id,
      'key': this.key,
      'plantingDate': this.plantingDate != null ? df.format(this.plantingDate) : null,
      'expectedHarvestDate': this.expectedHarvestDate != null ? df.format(this.expectedHarvestDate) : null,
      'actualHarvestDate': this.actualHarvestDate != null ? df.format(this.actualHarvestDate) : null,
      'harvested': this.harvested,
      'settings_id': this.settingsId,
      'observacoes': this.observacoes,
      'harvestedSpeciesId': this.harvestedSpeciesId
    };
  }

  factory Crop.fromJson(Map<String, dynamic> json) {
    DateFormat df = DateFormat('dd/MM/yyyy');
    return Crop(
      id: json['id'],
      key: json['key'],
      plantingDate: json['plantingDate'] == null ? null : df.parseUTC(json['plantingDate'] as String),
      expectedHarvestDate: json['expectedHarvestDate'] == null ? null : df.parseUTC(json['expectedHarvestDate'] as String),
      actualHarvestDate: json['actualHarvestDate'] == null ? null : df.parseUTC(json['actualHarvestDate'] as String),
      harvested: Crop._toBool(json['harvested']),
      settingsId: json['settings_id'] as int,
      observacoes: json['observacoes'] as String,
      harvestedSpeciesId: json['harvestedSpeciesId'] as int,
    );
  }
}
