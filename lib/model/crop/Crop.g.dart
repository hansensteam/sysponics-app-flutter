// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Crop.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Crop _$CropFromJson(Map<String, dynamic> json) {
  return Crop(
    id: json['id'],
    key: json['key'],
    plantingDate: json['plantingDate'] == null
        ? null
        : DateTime.parse(json['plantingDate'] as String),
    expectedHarvestDate: json['expectedHarvestDate'] == null
        ? null
        : DateTime.parse(json['expectedHarvestDate'] as String),
    actualHarvestDate: json['actualHarvestDate'] == null
        ? null
        : DateTime.parse(json['actualHarvestDate'] as String),
    harvested: Crop._toBool(json['harvested']),
    settingsId: json['settings_id'] as int,
    observacoes: json['observacoes'] as String,
    harvestedSpeciesId: json['harvestedSpeciesId'] as int,
  );
}

Map<String, dynamic> _$CropToJson(Crop instance) => <String, dynamic>{
      'id': instance.id,
      'key': instance.key,
      'plantingDate': instance.plantingDate?.toIso8601String(),
      'expectedHarvestDate': instance.expectedHarvestDate?.toIso8601String(),
      'actualHarvestDate': instance.actualHarvestDate?.toIso8601String(),
      'harvested': instance.harvested,
      'settings_id': instance.settingsId,
      'observacoes': instance.observacoes,
      'harvestedSpeciesId': instance.harvestedSpeciesId,
    };
