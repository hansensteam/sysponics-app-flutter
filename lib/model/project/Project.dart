import 'package:json_annotation/json_annotation.dart';
import 'package:sysponics/model/GenericModel.dart';

part 'Project.g.dart';

@JsonSerializable(disallowUnrecognizedKeys: false, ignoreUnannotated: true)
class Project extends GenericModel {
  @JsonKey(name: "name")
  String name;
  @JsonKey(name: "species_id")
  int speciesId;

  Project({id, key, this.name, this.speciesId}) : super(id, key);

  @override
  Map<String, dynamic> toMap() => _$ProjectToJson(this);

  factory Project.fromJson(Map<String, dynamic> json) => _$ProjectFromJson(json);
}
