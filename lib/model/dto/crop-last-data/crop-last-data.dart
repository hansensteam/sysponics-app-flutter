import 'package:json_annotation/json_annotation.dart';

part 'crop-last-data.g.dart';

@JsonSerializable(disallowUnrecognizedKeys: false, ignoreUnannotated: true)
class CropLastDataDTO {
  @JsonKey(name: 'airTemp')
  double airTemp;
  @JsonKey(name: 'airHum')
  double airHum;
  @JsonKey(name: 'waterTemp')
  double waterTemp;
  @JsonKey(name: 'waterPh')
  double waterPh;
  @JsonKey(name: 'lightsOn')
  bool lightsOn;
  @JsonKey(name: 'pumpOn')
  bool pumpOn;
  @JsonKey(name: 'waterLevelOk')
  bool waterLevelOk;

  CropLastDataDTO({this.airTemp, this.airHum, this.waterTemp, this.waterPh, this.lightsOn, this.pumpOn, this.waterLevelOk});

  Map<String, dynamic> toMap() => _$CropLastDataDTOToJson(this);

  factory CropLastDataDTO.fromJson(Map<String, dynamic> json) => _$CropLastDataDTOFromJson(json);
}
