// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'crop-last-data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CropLastDataDTO _$CropLastDataDTOFromJson(Map<String, dynamic> json) {
  return CropLastDataDTO(
    airTemp: (json['airTemp'] as num)?.toDouble(),
    airHum: (json['airHum'] as num)?.toDouble(),
    waterTemp: (json['waterTemp'] as num)?.toDouble(),
    waterPh: (json['waterPh'] as num)?.toDouble(),
    lightsOn: json['lightsOn'] as bool,
    pumpOn: json['pumpOn'] as bool,
    waterLevelOk: json['waterLevelOk'] as bool,
  );
}

Map<String, dynamic> _$CropLastDataDTOToJson(CropLastDataDTO instance) =>
    <String, dynamic>{
      'airTemp': instance.airTemp,
      'airHum': instance.airHum,
      'waterTemp': instance.waterTemp,
      'waterPh': instance.waterPh,
      'lightsOn': instance.lightsOn,
      'pumpOn': instance.pumpOn,
      'waterLevelOk': instance.waterLevelOk,
    };
