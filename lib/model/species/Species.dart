import 'package:json_annotation/json_annotation.dart';
import 'package:sysponics/model/GenericModel.dart';
import 'package:sysponics/model/species_names/SpeciesNames.dart';

part 'Species.g.dart';

@JsonSerializable(disallowUnrecognizedKeys: false, ignoreUnannotated: true)
class Species extends GenericModel {
  @JsonKey(name: "cientificName")
  String cientificName;

  @JsonKey(name: 'commonNames')
  List<SpeciesNames> commonNames;

  Species({id, key, this.cientificName, this.commonNames}) : super(id, key);

  @override
  Map<String, dynamic> toMap() => _$SpeciesToJson(this);

  factory Species.fromJson(Map<String, dynamic> json) => _$SpeciesFromJson(json);
}
