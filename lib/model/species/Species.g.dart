// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Species.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Species _$SpeciesFromJson(Map<String, dynamic> json) {
  return Species(
    id: json['id'],
    key: json['key'],
    cientificName: json['cientificName'] as String,
    commonNames: (json['commonNames'] as List)
        ?.map((e) =>
            e == null ? null : SpeciesNames.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$SpeciesToJson(Species instance) => <String, dynamic>{
      'id': instance.id,
      'key': instance.key,
      'cientificName': instance.cientificName,
      'commonNames': instance.commonNames,
    };
