class LoginData {
  String accessToken;
  String refreshToken;
  String tokenType;
  int expiresIn;
  String scope;

  LoginData(
      {this.accessToken,
      this.refreshToken,
      this.tokenType,
      this.expiresIn,
      this.scope});

  factory LoginData.fromJson(Map<String, dynamic> json) {
    return LoginData(
      accessToken: json['access_token'],
      refreshToken: json['refresh_token'],
      tokenType: json['token_type'],
      expiresIn: json['expires_in'],
      scope: json['scope'],
    );
  }
}
