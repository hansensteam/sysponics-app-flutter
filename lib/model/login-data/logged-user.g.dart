// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'logged-user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoggedUser _$LoggedUserFromJson(Map<String, dynamic> json) {
  return LoggedUser()
    ..id = json['id'] as int
    ..key = json['key'] as String
    ..nome = json['nome'] as String
    ..email = json['email'] as String
    ..username = json['username'] as String;
}

Map<String, dynamic> _$LoggedUserToJson(LoggedUser instance) =>
    <String, dynamic>{
      'id': instance.id,
      'key': instance.key,
      'nome': instance.nome,
      'email': instance.email,
      'username': instance.username,
    };
