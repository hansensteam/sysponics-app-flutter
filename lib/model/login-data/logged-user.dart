import 'package:json_annotation/json_annotation.dart';

part 'logged-user.g.dart';

@JsonSerializable(ignoreUnannotated: true, disallowUnrecognizedKeys: false)
class LoggedUser {
  @JsonKey(name: 'id')
  int id;
  @JsonKey(name: 'key')
  String key;
  @JsonKey(name: 'nome')
  String nome;
  @JsonKey(name: 'email')
  String email;
  @JsonKey(name: 'username')
  String username;

  LoggedUser();

  Map<String, dynamic> toMap() => _$LoggedUserToJson(this);

  factory LoggedUser.fromJson(Map<String, dynamic> json) =>
      _$LoggedUserFromJson(json);
}
