// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Device.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Device _$DeviceFromJson(Map<String, dynamic> json) {
  return Device(
    id: json['id'],
    key: json['key'],
    projectId: json['project_id'] as int,
    name: json['name'] as String,
    description: json['description'] as String,
    deviceType: json['deviceType'] as String,
  );
}

Map<String, dynamic> _$DeviceToJson(Device instance) => <String, dynamic>{
      'id': instance.id,
      'key': instance.key,
      'project_id': instance.projectId,
      'name': instance.name,
      'description': instance.description,
      'deviceType': instance.deviceType,
    };
