import 'package:json_annotation/json_annotation.dart';
import 'package:sysponics/model/GenericModel.dart';

part 'Device.g.dart';

@JsonSerializable(disallowUnrecognizedKeys: false, ignoreUnannotated: true)
class Device extends GenericModel {
  @JsonKey(name: "project_id")
  int projectId;
  @JsonKey(name: "name")
  String name;
  @JsonKey(name: "description")
  String description;
  @JsonKey(name: "deviceType")
  String deviceType;

  Device(
      {id, key, this.projectId, this.name, this.description, this.deviceType})
      : super(id, key);

  @override
  Map<String, dynamic> toMap() => _$DeviceToJson(this);

  factory Device.fromJson(Map<String, dynamic> json) => _$DeviceFromJson(json);
}

enum DeviceType {
  WATER_TEMP_SENSOR,
  WATER_PH_SENSOR,
  WATER_CE_SENSOR,
  AIR_TEMP_SENSOR,
  AIR_HUM_SENSOR,
  AIR_TEMP_HUM_SENSOR,
  ACT_LIGHTS,
  ACT_WATER_PUMP,
  ACT_AIR_PUMP,
  ACT_FAN,
  ACT_WATER_CHILLER,
  ACT_AIR_CHILLER,
  ACT_WATER_HEATER,
  ACT_AIR_HEATER,
  ACT_SPRINKLER,
  WATER_LEVEL_SENSOR
}

extension DeviceTypeExtension on DeviceType {
  String get value {
    return this.toString().substring(this.toString().indexOf('.') + 1);
  }

  String get description {
    switch (this) {
      case DeviceType.WATER_TEMP_SENSOR:
        return 'Sensor de Temperatura da Água';
      case DeviceType.WATER_PH_SENSOR:
        return 'Sensor de PH da Água';
      case DeviceType.WATER_CE_SENSOR:
        return 'Sensor de Condutividade Elétrica da Água';
      case DeviceType.AIR_TEMP_SENSOR:
        return 'Sensor de Temperatura do Ar';
      case DeviceType.AIR_HUM_SENSOR:
        return 'Sensor de Umidade do Ar';
      case DeviceType.AIR_TEMP_HUM_SENSOR:
        return 'Sensor de Umidade e Temperatura do Ar';
      case DeviceType.ACT_LIGHTS:
        return 'Iluminação';
      case DeviceType.ACT_WATER_PUMP:
        return 'Bomba d\'água';
      case DeviceType.ACT_AIR_PUMP:
        return 'Bomba de Ar';
      case DeviceType.ACT_FAN:
        return 'Ventilação';
      case DeviceType.ACT_WATER_CHILLER:
        return 'Arrefecimento Água';
      case DeviceType.ACT_AIR_CHILLER:
        return 'Arrefecimento Ar';
      case DeviceType.ACT_WATER_HEATER:
        return 'Aquecimento Água';
      case DeviceType.ACT_AIR_HEATER:
        return 'Aquecimento Ar';
      case DeviceType.ACT_SPRINKLER:
        return 'Sprinkler';
      case DeviceType.WATER_LEVEL_SENSOR:
        return 'Sensor de Nível de Água';
      default:
        return null;
    }
  }
}
