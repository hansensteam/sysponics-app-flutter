import 'package:json_annotation/json_annotation.dart';
import 'package:sysponics/model/GenericModel.dart';

part 'SpeciesNames.g.dart';

@JsonSerializable(disallowUnrecognizedKeys: false, ignoreUnannotated: true)
class SpeciesNames extends GenericModel {
  @JsonKey(name: "commonName")
  String commonName;
  @JsonKey(name: "locale")
  String locale;

  int speciesId;

  SpeciesNames({id, key, this.commonName, this.locale, this.speciesId}) : super(id, key);

  @override
  Map<String, dynamic> toMap() {
    return {'id': id, 'commonName': commonName, 'locale': locale, 'species_id': speciesId};
  }

  factory SpeciesNames.fromJson(Map<String, dynamic> json) => _$SpeciesNamesFromJson(json);
}
