// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'SpeciesNames.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SpeciesNames _$SpeciesNamesFromJson(Map<String, dynamic> json) {
  return SpeciesNames(
    id: json['id'],
    key: json['key'],
    commonName: json['commonName'] as String,
    locale: json['locale'] as String,
  );
}

Map<String, dynamic> _$SpeciesNamesToJson(SpeciesNames instance) =>
    <String, dynamic>{
      'id': instance.id,
      'key': instance.key,
      'commonName': instance.commonName,
      'locale': instance.locale,
    };
