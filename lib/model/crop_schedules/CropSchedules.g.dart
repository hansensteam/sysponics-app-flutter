// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'CropSchedules.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CropSchedules _$CropSchedulesFromJson(Map<String, dynamic> json) {
  return CropSchedules(
    id: json['id'],
    key: json['key'],
    settingsId: json['settings_id'] as int,
    startTime: json['startTime'] == null
        ? null
        : DateTime.parse(json['startTime'] as String),
    endTime: json['endTime'] == null
        ? null
        : DateTime.parse(json['endTime'] as String),
    scheduleType:
        _$enumDecodeNullable(_$ScheduleTypeEnumMap, json['scheduleType']),
    poweredOn: json['poweredOn'] as bool,
    value: (json['value'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$CropSchedulesToJson(CropSchedules instance) =>
    <String, dynamic>{
      'id': instance.id,
      'key': instance.key,
      'settings_id': instance.settingsId,
      'startTime': instance.startTime?.toIso8601String(),
      'endTime': instance.endTime?.toIso8601String(),
      'scheduleType': _$ScheduleTypeEnumMap[instance.scheduleType],
      'poweredOn': instance.poweredOn,
      'value': instance.value,
    };

T _$enumDecode<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }

  final value = enumValues.entries
      .singleWhere((e) => e.value == source, orElse: () => null)
      ?.key;

  if (value == null && unknownValue == null) {
    throw ArgumentError('`$source` is not one of the supported values: '
        '${enumValues.values.join(', ')}');
  }
  return value ?? unknownValue;
}

T _$enumDecodeNullable<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source, unknownValue: unknownValue);
}

const _$ScheduleTypeEnumMap = {
  ScheduleType.LIGHTS: 'LIGHTS',
  ScheduleType.WATER_TEMP: 'WATER_TEMP',
  ScheduleType.AIR_TEMP: 'AIR_TEMP',
  ScheduleType.AIR_HUM: 'AIR_HUM',
  ScheduleType.WATER_PUMP: 'WATER_PUMP',
};
