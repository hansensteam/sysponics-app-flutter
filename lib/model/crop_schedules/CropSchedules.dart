import 'package:charts_flutter/flutter.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:sysponics/model/GenericModel.dart';
import 'package:intl/intl.dart';

part 'CropSchedules.g.dart';

@JsonSerializable(disallowUnrecognizedKeys: false, ignoreUnannotated: true)
class CropSchedules extends GenericModel {
  @JsonKey(name: "settings_id")
  int settingsId;
  @JsonKey(name: "startTime")
  DateTime startTime;
  @JsonKey(name: "endTime")
  DateTime endTime;
  @JsonKey(name: "scheduleType")
  ScheduleType scheduleType;
  @JsonKey(name: "poweredOn")
  bool poweredOn;
  @JsonKey(name: "value")
  double value;

  CropSchedules({
    id,
    key,
    this.settingsId,
    this.startTime,
    this.endTime,
    this.scheduleType,
    this.poweredOn,
    this.value,
  }) : super(id, key);

  @override
  Map<String, dynamic> toMap() {
    DateFormat df = DateFormat('HH:mm:ss');
    String start = this.startTime != null ? df.format(this.startTime) : null;
    String end = this.endTime != null ? df.format(this.endTime) : null;
    return <String, dynamic>{
      'id': this.id,
      'key': this.key,
      'settings_id': this.settingsId,
      'startTime': start,
      'endTime': end,
      'scheduleType': _$ScheduleTypeEnumMap[this.scheduleType],
      'poweredOn': this.poweredOn,
      'value': this.value,
    };
  }

  factory CropSchedules.fromJson(Map<String, dynamic> json) {
    DateFormat df = DateFormat('HH:mm:ssZ', 'pt_BR');
    return CropSchedules(
      id: json['id'],
      key: json['key'],
      settingsId: json['settings_id'] as int,
      startTime: json['startTime'] == null ? null : df.parseUTC(json['startTime'] as String),
      endTime: json['endTime'] == null ? null : df.parseUTC(json['endTime'] as String),
      scheduleType: _$enumDecodeNullable(_$ScheduleTypeEnumMap, json['scheduleType']),
      poweredOn: json['poweredOn'] as bool,
      value: (json['value'] as num)?.toDouble(),
    );
  }
}

enum ScheduleType { LIGHTS, WATER_TEMP, AIR_TEMP, AIR_HUM, WATER_PUMP }

extension ScheduleTypeExtension on ScheduleType {
  String get description {
    switch (this) {
      case ScheduleType.WATER_TEMP:
        return 'Temperatura da Água';
      case ScheduleType.AIR_TEMP:
        return 'Temperatura do Ar';
      case ScheduleType.AIR_HUM:
        return 'Umidade do Ar';
      case ScheduleType.WATER_PUMP:
        return 'Bomba d\'água';
      default:
        return 'Iluminação';
    }
  }
}
