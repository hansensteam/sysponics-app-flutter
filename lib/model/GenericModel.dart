import 'package:json_annotation/json_annotation.dart';

abstract class GenericModel {
  @JsonKey(name: "id")
  int id;
  @JsonKey(name: "key")
  String key;

  GenericModel(
    int id,
    String key,
  )   : this.id = id,
        this.key = key;

  GenericModel.named(int id) {
    this.id = id;
  }

  Map<String, dynamic> toMap();
}
