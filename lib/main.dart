import 'package:flutter/material.dart';
import 'package:sysponics/assets/CustomColors.dart';
import 'package:sysponics/ui/devices/device-form.dart';
import 'package:sysponics/ui/devices/device-list-form.dart';
import 'package:sysponics/ui/login/login-form.dart';
import 'package:sysponics/ui/project/project-form.dart';
import 'package:sysponics/ui/project/project-list-form.dart';
import 'package:sysponics/ui/project/project-page/project-harvest-crop.dart';
import 'package:sysponics/ui/project/project-page/project-new-crop-page.dart';
import 'file:///E:/PROJECTS/Sysponics/sysponics_app_flutter/lib/ui/project/project-page/project-page-crops-widget.dart';
import 'file:///E:/PROJECTS/Sysponics/sysponics_app_flutter/lib/ui/project/project-page/project-page.dart';
import 'package:sysponics/ui/project/project-page/project-page-crops-historical-data-widget.dart';
import 'package:sysponics/ui/project/project-page/project-settings.dart';
import 'package:sysponics/ui/species/species-form.dart';
import 'package:sysponics/ui/species/species-list.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';

void main() => runApp(SysponicsApp());

class SysponicsApp extends StatelessWidget {
  initLocale() async {
    await initializeDateFormatting('pt_BR');
  }

  @override
  Widget build(BuildContext context) {
    Intl.defaultLocale = 'pt_BR';
    initLocale();

    return MaterialApp(
      title: 'Sysponics',
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: CustomColors.APP_BRAND_COLOR,
        hintColor: CustomColors.APP_BRAND_COLOR,
      ),
      initialRoute: LoginForm.route,
      routes: {
        LoginForm.route: (context) => LoginForm(),
        ProjectListForm.route: (context) => ProjectListForm(),
        ProjectPage.route: (context) => ProjectPage(),
        ProjectForm.route: (context) => ProjectForm(),
        DeviceListForm.route: (context) => DeviceListForm(),
        DeviceForm.route: (context) => DeviceForm(),
        ProjectPageCropsWidget.route: (context) => ProjectPageCropsWidget(),
        CropHistoricalDataWidget.route: (context) => CropHistoricalDataWidget(),
        ProjectSettingsPage.route: (context) => ProjectSettingsPage(),
        ProjectNewCropPage.route: (context) => ProjectNewCropPage(),
        SpeciesList.route: (context) => SpeciesList(),
        SpeciesForm.route: (context) => SpeciesForm(),
        ProjectHarvestCropPage.route: (context) => ProjectHarvestCropPage(),
      },
    );
  }
}
