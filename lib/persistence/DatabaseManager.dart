import 'dart:async';
import 'dart:io';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseManager {
  DatabaseManager._();

  static final DatabaseManager db = DatabaseManager._();
  static Database _database;

  Future<Database> get database async {
    if (_database != null) {
      return _database;
    }
    _database = await initDb();
    return _database;
  }

  initDb() async {
    Directory dir = await getApplicationDocumentsDirectory();
    String path = join(dir.path, 'sysponics.db');
    print(path);
    return await openDatabase(path, version: 1, onCreate: createDatabase);
  }

  Future<void> createDatabase(Database db, version) async {
    db
        .execute('CREATE TABLE species ( '
            'id INTEGER PRIMARY KEY, '
            'key TEXT, '
            'cientific_name TEXT )')
        .then((v) => print('CREATED TABLE species'), onError: (e) => print(e));
    db
        .execute('CREATE TABLE species_names ( '
            'id INTEGER PRIMARY KEY, '
            'key TEXT, '
            'common_name TEXT, '
            'locale TEXT, '
            'species_id bigint, '
            'FOREIGN KEY(species_id) REFERENCES species( id ) )')
        .then((v) => print('CREATED TABLE species_names'), onError: (e) => print(e));
    db
        .execute('CREATE TABLE project( '
            'id INTEGER PRIMARY KEY, '
            'key TEXT, '
            'name TEXT, '
            'species_id bigint, '
            'FOREIGN KEY(species_id) REFERENCES species( id ) )')
        .then((v) => print("CREATED TABLE project"), onError: (e) => print(e));
    db
        .execute('CREATE TABLE crop ( '
            'id INTEGER PRIMARY KEY, '
            'key TEXT, '
            'project_id bigint, '
            'plantingDate TEXT, '
            'expectedHarvestDate TEXT,'
            'actualHarvestDate TEXT,'
            'harvested INTEGER,'
            'settings_id INTEGER,'
            'FOREIGN KEY(project_id) REFERENCES project(id));')
        .then((v) => print("CREATED TABLE crop"), onError: (e) => print(e));
    db
        .execute('CREATE TABLE crop_settings('
            'id INTEGER PRIMARY KEY,'
            'key TEXT,'
            'waterPh REAL,'
            'millisToVerify INTEGER,'
            'crop_id INTEGER,'
            'FOREIGN KEY(crop_id) REFERENCES crop(id))')
        .then((v) => print("CREATED TABLE crop_settings"), onError: (e) => print(e));
    db
        .execute('CREATE TABLE crop_schedules('
            'id INTEGER PRIMARY KEY,'
            'key TEXT,'
            'settings_id INTEGER,'
            'startTime TEXT,'
            'endTime TEXT,'
            'scheduleType TEXT,'
            'poweredOn INTEGER,'
            'value REAL,'
            'FOREIGN KEY(settings_id) REFERENCES crop_settings(id))')
        .then((v) => print("CREATED TABLE crop_schedules"), onError: (e) => print(e));
    db
        .execute('CREATE TABLE device( '
            'id INTEGER PRIMARY KEY,'
            'key TEXT,'
            'name TEXT,'
            'description TEXT,'
            'deviceType TEXT,'
            'project_id INTEGER,'
            'FOREIGN KEY(project_id) REFERENCES project(id) )')
        .then((v) => print('CREATED TABLE device'), onError: (e) => print(e));
    db
        .execute('CREATE TABLE historical_data('
            'id INTEGER PRIMARY KEY,'
            'key TEXT,'
            'device_id INTEGER,'
            'crop_id INTEGER,'
            'value REAL,'
            'timestamp TEXT,'
            'type TEXT,'
            'FOREIGN KEY(device_id) REFERENCES device(id),'
            'FOREIGN KEY(crop_id) REFERENCES crop(id))')
        .then((v) => print("CREATED TABLE historical_data"), onError: (e) => print(e));
  }
}
