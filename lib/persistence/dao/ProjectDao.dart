import 'package:sysponics/model/project/Project.dart';

import 'GenericDao.dart';

class ProjectDao extends GenericDao<Project>{
  @override
  convertToObj(Map<String, dynamic> map) {
    return Project.fromJson(map);
  }

  @override
  String tableName() {
    return "project";
  }

}