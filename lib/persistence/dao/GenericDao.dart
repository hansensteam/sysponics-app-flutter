import 'package:sqflite/sqflite.dart';
import 'package:sysponics/model/GenericModel.dart';
import 'package:sysponics/persistence/DatabaseManager.dart';

abstract class GenericDao<T extends GenericModel> {
  T convertToObj(Map<String, dynamic> map);

  String tableName();

  Future<int> insert(T t) async {
    final Database db = await DatabaseManager.db.database;
    return await db.insert(tableName(), t.toMap(), conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<void> update(T t) async {
    final db = await DatabaseManager.db.database;
    await db.update(
      tableName(),
      t.toMap(),
      where: 'id = ?',
      whereArgs: [t.id],
    );
  }

  Future<void> delete(int id) async {
    final db = await DatabaseManager.db.database;
    await db.delete(tableName(), where: 'id=?', whereArgs: [id]);
  }

  Future<List<T>> findAll() async {
    final db = await DatabaseManager.db.database;
    final List<Map<String, dynamic>> maps = await db.query(tableName());
    return List.generate(maps.length, (i) {
      return convertToObj(maps[i]);
    });
  }

  Future<T> findById(int id) async {
    final db = await DatabaseManager.db.database;
    List<Map<String, dynamic>> maps = await db.query(
      tableName(),
      where: 'id=?',
      whereArgs: [id],
    );
    List<T> list = List.generate(maps.length, (i) {
      return convertToObj(maps[i]);
    });
    if (list.length > 0) {
      return list[0];
    } else {
      return null;
    }
  }
}
