import 'package:sysponics/model/species_names/SpeciesNames.dart';
import 'package:sysponics/persistence/dao/GenericDao.dart';

import '../../DatabaseManager.dart';

class SpeciesNamesDao extends GenericDao<SpeciesNames> {
  @override
  SpeciesNames convertToObj(Map<String, dynamic> map) {
    return SpeciesNames(
      id: map['id'],
      commonName: map['common_name'],
      locale: map['locale'],
      speciesId: map['species_id'],
    );
  }

  @override
  String tableName() {
    return 'species_names';
  }

  Future<List<SpeciesNames>> findBySpeciesId(int id) async {
    final db = await DatabaseManager.db.database;
    List<Map<String, dynamic>> maps = await db.query(
      tableName(),
      where: 'species_id=?',
      whereArgs: [id],
    );
    return List.generate(maps.length, (i) {
      return convertToObj(maps[i]);
    });
  }
}
