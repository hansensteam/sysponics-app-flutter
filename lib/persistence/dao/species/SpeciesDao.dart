import 'package:sysponics/model/species/Species.dart';

import '../GenericDao.dart';

class SpeciesDao extends GenericDao<Species>{
  @override
  Species convertToObj(Map<String, dynamic> map) {
    return Species(id: map['id'], cientificName: map['cientific_name']);
  }

  @override
  String tableName() {
    return 'species';
  }



}