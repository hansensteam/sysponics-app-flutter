import 'package:sysponics/model/device/Device.dart';
import 'package:sysponics/persistence/dao/GenericDao.dart';

class DeviceDao extends GenericDao<Device>{
  @override
  Device convertToObj(Map<String, dynamic> map) {
    return Device.fromJson(map);
  }

  @override
  String tableName() {
    return "device";
  }
}