import 'package:sysponics/model/data/HistoricalData.dart';
import 'package:sysponics/persistence/DatabaseManager.dart';
import 'package:sysponics/persistence/dao/GenericDao.dart';

class HistoricalDataDao extends GenericDao<HistoricalData> {
  @override
  HistoricalData convertToObj(Map<String, dynamic> map) {
    return HistoricalData.fromJson(map);
  }

  @override
  String tableName() {
    return "historical_data";
  }

  Future<List<HistoricalData>> findAllByDeviceId(int deviceId) async {
    final db = await DatabaseManager.db.database;
    final List<Map<String, dynamic>> list = await db.query(
      tableName(),
      where: "device_id=?",
      whereArgs: [deviceId],
    );
    return List.generate(list.length, (i) {
      return HistoricalData.fromJson(list[i]);
    });
  }

  Future<List<HistoricalData>> findAllByCropId(int cropId) async {
    final db = await DatabaseManager.db.database;
    final List<Map<String, dynamic>> list = await db.query(
      tableName(),
      where: "crop_id=?",
      whereArgs: [cropId],
    );
    return List.generate(list.length, (i) {
      return HistoricalData.fromJson(list[i]);
    });
  }
}
