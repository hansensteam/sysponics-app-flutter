import 'package:sysponics/model/crop_settings/CropSettings.dart';
import 'package:sysponics/persistence/DatabaseManager.dart';
import 'package:sysponics/persistence/dao/GenericDao.dart';

class CropSettingsDao extends GenericDao<CropSettings> {
  @override
  CropSettings convertToObj(Map<String, dynamic> map) {
    return CropSettings.fromJson(map);
  }

  @override
  String tableName() {
    return "crop_settings";
  }

  Future<CropSettings> findByCropId(int cropId) async {
    final db = await DatabaseManager.db.database;
    final List<Map<String, dynamic>> list = await db.query(
      tableName(),
      where: 'crop_id=?',
      whereArgs: [cropId],
    );

  }
}
