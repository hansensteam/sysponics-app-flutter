import 'package:sysponics/model/crop_schedules/CropSchedules.dart';
import 'package:sysponics/persistence/DatabaseManager.dart';

import '../GenericDao.dart';

class CropSchedulesDao extends GenericDao<CropSchedules> {
  @override
  CropSchedules convertToObj(Map<String, dynamic> map) {
    return CropSchedules.fromJson(map);
  }

  @override
  String tableName() {
    return "crop_schedules";
  }

  Future<List<CropSchedules>> findAllBySettingsId(int settingsId) async {
    final db = await DatabaseManager.db.database;
    final List<Map<String, dynamic>> list = await db.query(
      tableName(),
      where: 'settings_id=?',
      whereArgs: [settingsId],
    );
    return List.generate(list.length, (i) => CropSchedules.fromJson(list[i]));
  }
}
