import 'package:sysponics/model/crop/Crop.dart';
import 'package:sysponics/persistence/dao/GenericDao.dart';

class CropDao extends GenericDao<Crop>{
  @override
  Crop convertToObj(Map<String, dynamic> map) {
    return Crop.fromJson(map);
  }

  @override
  String tableName() {
    return "crop";
  }

}